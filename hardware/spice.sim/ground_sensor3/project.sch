EESchema Schematic File Version 2
LIBS:kicad-spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L V V1
U 1 1 54ECA53D
P 7000 3200
F 0 "V1" V 6800 3200 60  0000 C CNN
F 1 "DC 3.3V" V 7250 3200 60  0000 C CNN
F 2 "" H 7000 3200 60  0000 C CNN
F 3 "" H 7000 3200 60  0000 C CNN
	1    7000 3200
	1    0    0    -1  
$EndComp
Text GLabel 7000 2900 1    60   Input ~ 0
VCC
Text Notes 850  2800 0    60   ~ 0
-pspice\n* \n.SUBCKT 1N4148 1 2 \n*\n* The resistor R1 does not reflect \n* a physical device. Instead it\n* improves modeling in the reverse \n* mode of operation.\n*\nR1 1 2 5.827E+9 \nD1 1 2 1N4148\n*\n.MODEL 1N4148 D \n+ IS = 4.352E-9 \n+ N = 1.906 \n+ BV = 110 \n+ IBV = 0.0001 \n+ RS = 0.6458 \n+ CJO = 7.048E-13 \n+ VJ = 0.869 \n+ M = 0.03 \n+ FC = 0.5 \n+ TT = 3.48E-9 \n.ENDS
Text Notes 800  7550 0    60   ~ 0
-pspice\n* \n.MODEL QBC807_40 PNP\n+ IS = 2.077E-13 \n+ NF = 1.005 \n+ ISE = 1.411E-14 \n+ NE = 1.3 \n+ BF = 449.8 \n+ IKF = 0.36 \n+ VAF = 29 \n+ NR = 1.002 \n+ ISC = 2.963E-13 \n+ NC = 1.25 \n+ BR = 20.92 \n+ IKR = 0.104 \n+ VAR = 10 \n+ RB = 40 \n+ IRB = 1E-05 \n+ RBM = 5.3 \n+ RE = 0.14 \n+ RC = 0.32 \n+ XTB = 0 \n+ EG = 1.11 \n+ XTI = 3 \n+ CJE = 5E-11 \n+ VJE = 0.9296 \n+ MJE = 0.456 \n+ TF = 7E-10 \n+ XTF = 3.25 \n+ VTF = 2.5 \n+ ITF = 0.79 \n+ PTF = 80 \n+ CJC = 2.675E-11 \n+ VJC = 0.8956 \n+ MJC = 0.4638 \n+ XCJC =  0.459 \n+ TR = 3.5E-08 \n+ CJS = 0 \n+ VJS = 0.75 \n+ MJS = 0.333 \n+ FC = 0.935
Text Notes 2100 7550 0    60   ~ 0
-pspice\n* \n.MODEL QSS9012 PNP\n+is=2.0417e-14\n+bf=143.3\n+vaf=47.75\n+ikf=0.5743\n+ise=75.86f\n+ne=2\n+br=14.345\n+nr=0.999\n+var=86.14\n+ikr=0.4265\n+isc=1.585f\n+nc=1.0087\n+rb=57.5\n+irb=7.943u\n+rbm=8.09\n+re=0.02\n+rc=0.743\n+cje=35.1p\n+vje=0.866\n+mje=0.411\n+cjc=19.1p\n+vjc=0.787\n+mjc=0.394\n+xcjc=0.349\n+xtb=1.413\n+eg=1.0885\n+xti=3\n+fc=0.5\n+Vceo=20\n+Icrating=0.5
$Comp
L 0 #GND01
U 1 1 550C7393
P 7000 3500
F 0 "#GND01" H 7000 3400 40  0001 C CNN
F 1 "0" H 7000 3430 40  0000 C CNN
F 2 "" H 7000 3500 60  0000 C CNN
F 3 "" H 7000 3500 60  0000 C CNN
	1    7000 3500
	1    0    0    -1  
$EndComp
Text Notes 8450 3200 0    60   ~ 0
-pspice\n* \n.MODEL Q2N3904  NPN\n+ IS=4.9148E-15\n+ BF=191.70\n+ VAF=100\n+ IKF=.28579\n+ ISE=11.882E-15\n+ NE=1.4422\n+ BR=5.6808\n+ VAR=100\n+ IKR=61.753E-3\n+ ISC=71.145E-12\n+ NC=1.6595\n+ NK=.8296\n+ RB=5.8072\n+ RC=.70808\n+ CJE=6.9435E-12\n+ VJE=.61872\n+ MJE=.27802\n+ CJC=3.7572E-12\n+ VJC=1.2237\n+ MJC=.28886\n+ TF=523.89E-12\n+ XTF=83.066\n+ VTF=67.769\n+ ITF=1.8804\n+ TR=10.000E-9
Text Notes 7700 6050 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 10nS 2mS 0mS\n*  tran 10nS 50.1mS 50mS\n*  tran 1nS 1.1mS 1mS\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(mon)\n  + v(gfb)\n  + v(out)\n.endc
Text Notes 4250 1950 0    60   ~ 0
-pspice\n*\n* Model generated on Feb 28, 13\n* MODEL FORMAT: PSpice\n.MODEL Q2n2222a npn\n+IS=3.88184e-14 BF=929.846 NF=1.10496 VAF=16.5003\n+IKF=0.019539 ISE=1.0168e-11 NE=1.94752 BR=48.4545\n+NR=1.07004 VAR=40.538 IKR=0.19539 ISC=1.0168e-11\n+NC=4 RB=0.1 IRB=0.1 RBM=0.1\n+RE=0.0001 RC=0.426673 XTB=0.1 XTI=1\n+EG=1.05 CJE=2.23677e-11 VJE=0.582701 MJE=0.63466\n+TF=4.06711e-10 XTF=3.92912 VTF=17712.6 ITF=0.4334\n+CJC=2.23943e-11 VJC=0.576146 MJC=0.632796 XCJC=1\n+FC=0.170253 CJS=0 VJS=0.75 MJS=0.5\n+TR=1e-07 PTF=0 KF=0 AF=1
$Comp
L V V2
U 1 1 572C2687
P 4000 3500
F 0 "V2" V 3800 3500 60  0000 C CNN
F 1 "PULSE 0V 3.2V 0S 0S 0S 108nS 216nS" H 3650 3950 47  0000 C CNN
F 2 "" H 4000 3500 60  0000 C CNN
F 3 "" H 4000 3500 60  0000 C CNN
	1    4000 3500
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 572C2897
P 7100 4400
F 0 "R2" H 7100 4500 50  0000 C CNN
F 1 "680K" H 7100 4300 50  0000 C CNN
F 2 "" V 7030 4400 30  0000 C CNN
F 3 "" H 7100 4400 30  0000 C CNN
	1    7100 4400
	0    -1   -1   0   
$EndComp
Text GLabel 5200 4100 0    60   Input ~ 0
GFB
Wire Wire Line
	7000 3400 7000 3500
Wire Wire Line
	7000 2900 7000 3000
Wire Wire Line
	5200 4100 5300 4100
Wire Wire Line
	5300 4100 5300 4300
Wire Wire Line
	4000 3300 4000 3200
Wire Wire Line
	4000 3200 4100 3200
Text GLabel 4100 3200 2    60   Input ~ 0
MON
$Comp
L 0 #GND02
U 1 1 572C3390
P 4000 3800
F 0 "#GND02" H 4000 3700 40  0001 C CNN
F 1 "0" H 4000 3730 40  0000 C CNN
F 2 "" H 4000 3800 60  0000 C CNN
F 3 "" H 4000 3800 60  0000 C CNN
	1    4000 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3700 4000 3800
$Comp
L D X1
U 1 1 573A0939
P 6300 4100
F 0 "X1" H 6300 4225 60  0000 C CNN
F 1 "1N4148" H 6300 3975 60  0000 C CNN
F 2 "" H 6300 4100 60  0000 C CNN
F 3 "" H 6300 4100 60  0000 C CNN
	1    6300 4100
	1    0    0    -1  
$EndComp
$Comp
L D X2
U 1 1 573A0A1D
P 6000 4400
F 0 "X2" H 6000 4525 60  0000 C CNN
F 1 "1N4148" H 6000 4275 60  0000 C CNN
F 2 "" H 6000 4400 60  0000 C CNN
F 3 "" H 6000 4400 60  0000 C CNN
	1    6000 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 4100 6000 4100
Wire Wire Line
	6000 4100 6000 4300
$Comp
L C C2
U 1 1 573A0B8B
P 5300 4400
F 0 "C2" H 5300 4525 60  0000 C CNN
F 1 "5.8p" H 5300 4275 60  0000 C CNN
F 2 "" H 4650 4950 60  0000 C CNN
F 3 "" H 4650 4950 60  0000 C CNN
	1    5300 4400
	0    -1   -1   0   
$EndComp
Text GLabel 5800 3200 2    60   Input ~ 0
GFB
$Comp
L R R1
U 1 1 573A0DAD
P 5100 3200
F 0 "R1" H 5100 3300 50  0000 C CNN
F 1 "10K" H 5100 3100 50  0000 C CNN
F 2 "" V 5030 3200 30  0000 C CNN
F 3 "" H 5100 3200 30  0000 C CNN
	1    5100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3200 4800 3200
Text GLabel 4800 3200 0    60   Input ~ 0
MON
$Comp
L C C1
U 1 1 573A0ED9
P 5600 3200
F 0 "C1" H 5600 3325 60  0000 C CNN
F 1 "10n" H 5600 3075 60  0000 C CNN
F 2 "" H 4950 3750 60  0000 C CNN
F 3 "" H 4950 3750 60  0000 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3200 5300 3200
Wire Wire Line
	5700 3200 5800 3200
Text GLabel 6000 4100 0    60   Input ~ 0
GFB
Wire Wire Line
	6000 4500 6000 4700
$Comp
L 0 #GND03
U 1 1 573A1124
P 6000 4700
F 0 "#GND03" H 6000 4600 40  0001 C CNN
F 1 "0" H 6000 4630 40  0000 C CNN
F 2 "" H 6000 4700 60  0000 C CNN
F 3 "" H 6000 4700 60  0000 C CNN
	1    6000 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4500 5300 4700
$Comp
L 0 #GND04
U 1 1 573A11C1
P 5300 4700
F 0 "#GND04" H 5300 4600 40  0001 C CNN
F 1 "0" H 5300 4630 40  0000 C CNN
F 2 "" H 5300 4700 60  0000 C CNN
F 3 "" H 5300 4700 60  0000 C CNN
	1    5300 4700
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 573A12D5
P 6600 4400
F 0 "C3" H 6600 4525 60  0000 C CNN
F 1 "10n" H 6600 4275 60  0000 C CNN
F 2 "" H 5950 4950 60  0000 C CNN
F 3 "" H 5950 4950 60  0000 C CNN
	1    6600 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 4100 7100 4100
Wire Wire Line
	6600 4100 6600 4300
Wire Wire Line
	6600 4500 6600 4700
$Comp
L 0 #GND05
U 1 1 573A13A2
P 6600 4700
F 0 "#GND05" H 6600 4600 40  0001 C CNN
F 1 "0" H 6600 4630 40  0000 C CNN
F 2 "" H 6600 4700 60  0000 C CNN
F 3 "" H 6600 4700 60  0000 C CNN
	1    6600 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4100 7100 4200
Connection ~ 6600 4100
Wire Wire Line
	7100 4600 7100 4700
$Comp
L 0 #GND06
U 1 1 573A141F
P 7100 4700
F 0 "#GND06" H 7100 4600 40  0001 C CNN
F 1 "0" H 7100 4630 40  0000 C CNN
F 2 "" H 7100 4700 60  0000 C CNN
F 3 "" H 7100 4700 60  0000 C CNN
	1    7100 4700
	1    0    0    -1  
$EndComp
Text GLabel 7100 4100 2    60   Input ~ 0
OUT
$Comp
L R R3
U 1 1 573A1944
P 7500 3200
F 0 "R3" H 7500 3300 50  0000 C CNN
F 1 "1M" H 7500 3100 50  0000 C CNN
F 2 "" V 7430 3200 30  0000 C CNN
F 3 "" H 7500 3200 30  0000 C CNN
	1    7500 3200
	0    -1   -1   0   
$EndComp
Text GLabel 7500 2900 1    60   Input ~ 0
VCC
Wire Wire Line
	7500 2900 7500 3000
$Comp
L 0 #GND07
U 1 1 573A19F3
P 7500 3500
F 0 "#GND07" H 7500 3400 40  0001 C CNN
F 1 "0" H 7500 3430 40  0000 C CNN
F 2 "" H 7500 3500 60  0000 C CNN
F 3 "" H 7500 3500 60  0000 C CNN
	1    7500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3400 7500 3500
Text GLabel 5400 3200 1    60   Input ~ 0
X1
Text Notes 2900 2600 0    60   ~ 0
* 1MHz\nPULSE 0V 3.2V 0S 0S 0S 0.5mS 1mS\n* 4.8MHz\nPULSE 0V 3.2V 0S 0S 0S 108nS 216nS
Text Notes 4300 4900 0    60   ~ 0
2.3p - dry\n5.8p - wet
$EndSCHEMATC
