EESchema Schematic File Version 2
LIBS:kicad-spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L V V1
U 1 1 54ECA53D
P 7200 3800
F 0 "V1" V 7000 3800 60  0000 C CNN
F 1 "DC 12V" V 7450 3800 60  0000 C CNN
F 2 "" H 7200 3800 60  0000 C CNN
F 3 "" H 7200 3800 60  0000 C CNN
	1    7200 3800
	1    0    0    -1  
$EndComp
Text GLabel 7200 3500 1    60   Input ~ 0
VCC
Text Notes 850  2800 0    60   ~ 0
-pspice\n* \n.SUBCKT 1N4148 1 2 \n*\n* The resistor R1 does not reflect \n* a physical device. Instead it\n* improves modeling in the reverse \n* mode of operation.\n*\nR1 1 2 5.827E+9 \nD1 1 2 1N4148\n*\n.MODEL 1N4148 D \n+ IS = 4.352E-9 \n+ N = 1.906 \n+ BV = 110 \n+ IBV = 0.0001 \n+ RS = 0.6458 \n+ CJO = 7.048E-13 \n+ VJ = 0.869 \n+ M = 0.03 \n+ FC = 0.5 \n+ TT = 3.48E-9 \n.ENDS
Text Notes 1100 6950 0    60   ~ 0
-pspice\n* \n.MODEL QSS9013 NPN\n+ IS=34.068E-15\n+ BF=200\n+ VAF=67\n+ IKF=1.1640\n+ ISE=12.368E-15\n+ NE=2\n+ BR=15.173\n+ VAR=40.840\n+ IKR=.26135\n+ ISC=1.9055E-15\n+ NC=1.0660\n+ RE=20.000E-3\n+ RB=63.200\n+ RC=.7426\n+ CJE=35.300E-12\n+ VJE=.808\n+ MJE=.372\n+ CJC=17.400E-12\n+ VJC=.614\n+ MJC=.388\n+ TF=10.000E-9\n+ XTF=10\n+ VTF=10\n+ ITF=1\n+ TR=10.000E-9\n+ EG=1.0999\n+ XTB=1.4025
$Comp
L 0 #GND01
U 1 1 550C7393
P 7200 4100
F 0 "#GND01" H 7200 4000 40  0001 C CNN
F 1 "0" H 7200 4030 40  0000 C CNN
F 2 "" H 7200 4100 60  0000 C CNN
F 3 "" H 7200 4100 60  0000 C CNN
	1    7200 4100
	1    0    0    -1  
$EndComp
Text Notes 8450 3200 0    60   ~ 0
-pspice\n* \n.MODEL Q2N3904  NPN\n+ IS=4.9148E-15\n+ BF=191.70\n+ VAF=100\n+ IKF=.28579\n+ ISE=11.882E-15\n+ NE=1.4422\n+ BR=5.6808\n+ VAR=100\n+ IKR=61.753E-3\n+ ISC=71.145E-12\n+ NC=1.6595\n+ NK=.8296\n+ RB=5.8072\n+ RC=.70808\n+ CJE=6.9435E-12\n+ VJE=.61872\n+ MJE=.27802\n+ CJC=3.7572E-12\n+ VJC=1.2237\n+ MJC=.28886\n+ TF=523.89E-12\n+ XTF=83.066\n+ VTF=67.769\n+ ITF=1.8804\n+ TR=10.000E-9
Text Notes 7700 6050 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 1uS 50mS 0mS\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(ctl)\n  + v(out)\n  + v(drv,mea)/0.001\n.endc
$Comp
L V V2
U 1 1 572C2687
P 4500 4900
F 0 "V2" V 4300 4900 60  0000 C CNN
F 1 "PWL 0mS 0V 10mS 0V 11mS 3.2V 29mS 3.2V 30mS 0V 40mS 0V" H 4150 5350 47  0000 C CNN
F 2 "" H 4500 4900 60  0000 C CNN
F 3 "" H 4500 4900 60  0000 C CNN
	1    4500 4900
	1    0    0    -1  
$EndComp
$Comp
L QNPN Q1
U 1 1 572C2A8B
P 6200 4600
F 0 "Q1" H 6200 4800 60  0000 C CNN
F 1 "QSS9013" V 6450 4600 60  0000 C CNN
F 2 "" H 5875 4600 60  0000 C CNN
F 3 "" H 5875 4600 60  0000 C CNN
	1    6200 4600
	1    0    0    -1  
$EndComp
Text GLabel 5400 4600 0    60   Input ~ 0
CTL
Text GLabel 6300 1900 1    60   Input ~ 0
VCC
$Comp
L 0 #GND02
U 1 1 572C2F5D
P 6300 4900
F 0 "#GND02" H 6300 4800 40  0001 C CNN
F 1 "0" H 6300 4830 40  0000 C CNN
F 2 "" H 6300 4900 60  0000 C CNN
F 3 "" H 6300 4900 60  0000 C CNN
	1    6300 4900
	1    0    0    -1  
$EndComp
Text GLabel 6300 3100 2    60   Input ~ 0
DRV
Wire Wire Line
	7200 4000 7200 4100
Wire Wire Line
	7200 3500 7200 3600
Wire Wire Line
	6000 4600 5900 4600
Wire Wire Line
	6300 3000 6300 3200
Wire Wire Line
	6300 4800 6300 4900
Wire Wire Line
	4500 4700 4500 4600
Wire Wire Line
	4500 4600 4600 4600
Text GLabel 4600 4600 2    60   Input ~ 0
CTL
$Comp
L 0 #GND03
U 1 1 572C3390
P 4500 5200
F 0 "#GND03" H 4500 5100 40  0001 C CNN
F 1 "0" H 4500 5130 40  0000 C CNN
F 2 "" H 4500 5200 60  0000 C CNN
F 3 "" H 4500 5200 60  0000 C CNN
	1    4500 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5100 4500 5200
$Comp
L R R1
U 1 1 572C434C
P 6300 2800
F 0 "R1" H 6300 2900 50  0000 C CNN
F 1 "26" H 6300 2700 50  0000 C CNN
F 2 "" V 6230 2800 30  0000 C CNN
F 3 "" H 6300 2800 30  0000 C CNN
	1    6300 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 1900 6300 2100
$Comp
L R R3
U 1 1 572C44B3
P 5700 4600
F 0 "R3" H 5700 4700 50  0000 C CNN
F 1 "240" H 5700 4500 50  0000 C CNN
F 2 "" V 5630 4600 30  0000 C CNN
F 3 "" H 5700 4600 30  0000 C CNN
	1    5700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4600 5400 4600
$Comp
L D X1
U 1 1 572C4602
P 5900 2800
F 0 "X1" H 5900 2925 60  0000 C CNN
F 1 "1N4148" H 5900 2675 60  0000 C CNN
F 2 "" H 5900 2800 60  0000 C CNN
F 3 "" H 5900 2800 60  0000 C CNN
	1    5900 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 2900 5900 3100
Wire Wire Line
	5900 3100 6300 3100
Connection ~ 6300 3100
Wire Wire Line
	5900 2000 5900 2700
Wire Wire Line
	5900 2000 6300 2000
Connection ~ 6300 2000
$Comp
L R R2
U 1 1 572C476B
P 6300 4000
F 0 "R2" H 6300 4100 60  0000 C CNN
F 1 "26" H 6300 3900 60  0000 C CNN
F 2 "" H 6300 4000 60  0000 C CNN
F 3 "" H 6300 4000 60  0000 C CNN
	1    6300 4000
	0    -1   -1   0   
$EndComp
$Comp
L C C1
U 1 1 572C47C4
P 5900 4000
F 0 "C1" H 5900 4125 60  0000 C CNN
F 1 "47u" H 5900 3875 60  0000 C CNN
F 2 "" H 5250 4550 60  0000 C CNN
F 3 "" H 5250 4550 60  0000 C CNN
	1    5900 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 4200 6300 4400
Wire Wire Line
	6300 4300 5900 4300
Wire Wire Line
	5900 4300 5900 4100
Connection ~ 6300 4300
Wire Wire Line
	5900 3900 5900 3700
Wire Wire Line
	5900 3700 6300 3700
Connection ~ 6300 3700
$Comp
L L L1
U 1 1 572C4B8C
P 6300 2300
F 0 "L1" H 6300 2400 60  0000 C CNN
F 1 "17mH" H 6300 2250 60  0000 C CNN
F 2 "" H 6300 2300 60  0000 C CNN
F 3 "" H 6300 2300 60  0000 C CNN
	1    6300 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 2500 6300 2600
Text GLabel 6300 4300 2    60   Input ~ 0
OUT
$Comp
L R R4
U 1 1 572C5682
P 6300 3400
F 0 "R4" H 6300 3500 60  0000 C CNN
F 1 "0.001" H 6300 3300 60  0000 C CNN
F 2 "" H 6300 3400 60  0000 C CNN
F 3 "" H 6300 3400 60  0000 C CNN
	1    6300 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 3600 6300 3800
Text GLabel 6300 3700 2    60   Input ~ 0
MEA
$EndSCHEMATC
