EESchema Schematic File Version 2
LIBS:kicad-spice
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L V V1
U 1 1 54ECA53D
P 6600 3200
F 0 "V1" V 6400 3200 60  0000 C CNN
F 1 "DC 3.3V" V 6850 3200 60  0000 C CNN
F 2 "" H 6600 3200 60  0000 C CNN
F 3 "" H 6600 3200 60  0000 C CNN
	1    6600 3200
	1    0    0    -1  
$EndComp
Text GLabel 6600 2900 1    60   Input ~ 0
VCC
Text Notes 850  2800 0    60   ~ 0
-pspice\n* \n.SUBCKT 1N4148 1 2 \n*\n* The resistor R1 does not reflect \n* a physical device. Instead it\n* improves modeling in the reverse \n* mode of operation.\n*\nR1 1 2 5.827E+9 \nD1 1 2 1N4148\n*\n.MODEL 1N4148 D \n+ IS = 4.352E-9 \n+ N = 1.906 \n+ BV = 110 \n+ IBV = 0.0001 \n+ RS = 0.6458 \n+ CJO = 7.048E-13 \n+ VJ = 0.869 \n+ M = 0.03 \n+ FC = 0.5 \n+ TT = 3.48E-9 \n.ENDS
Text Notes 800  7550 0    60   ~ 0
-pspice\n* \n.MODEL QBC807_40 PNP\n+ IS = 2.077E-13 \n+ NF = 1.005 \n+ ISE = 1.411E-14 \n+ NE = 1.3 \n+ BF = 449.8 \n+ IKF = 0.36 \n+ VAF = 29 \n+ NR = 1.002 \n+ ISC = 2.963E-13 \n+ NC = 1.25 \n+ BR = 20.92 \n+ IKR = 0.104 \n+ VAR = 10 \n+ RB = 40 \n+ IRB = 1E-05 \n+ RBM = 5.3 \n+ RE = 0.14 \n+ RC = 0.32 \n+ XTB = 0 \n+ EG = 1.11 \n+ XTI = 3 \n+ CJE = 5E-11 \n+ VJE = 0.9296 \n+ MJE = 0.456 \n+ TF = 7E-10 \n+ XTF = 3.25 \n+ VTF = 2.5 \n+ ITF = 0.79 \n+ PTF = 80 \n+ CJC = 2.675E-11 \n+ VJC = 0.8956 \n+ MJC = 0.4638 \n+ XCJC =  0.459 \n+ TR = 3.5E-08 \n+ CJS = 0 \n+ VJS = 0.75 \n+ MJS = 0.333 \n+ FC = 0.935
Text Notes 2100 7550 0    60   ~ 0
-pspice\n* \n.MODEL QSS9012 PNP\n+is=2.0417e-14\n+bf=143.3\n+vaf=47.75\n+ikf=0.5743\n+ise=75.86f\n+ne=2\n+br=14.345\n+nr=0.999\n+var=86.14\n+ikr=0.4265\n+isc=1.585f\n+nc=1.0087\n+rb=57.5\n+irb=7.943u\n+rbm=8.09\n+re=0.02\n+rc=0.743\n+cje=35.1p\n+vje=0.866\n+mje=0.411\n+cjc=19.1p\n+vjc=0.787\n+mjc=0.394\n+xcjc=0.349\n+xtb=1.413\n+eg=1.0885\n+xti=3\n+fc=0.5\n+Vceo=20\n+Icrating=0.5
$Comp
L 0 #GND01
U 1 1 550C7393
P 6600 3500
F 0 "#GND01" H 6600 3400 40  0001 C CNN
F 1 "0" H 6600 3430 40  0000 C CNN
F 2 "" H 6600 3500 60  0000 C CNN
F 3 "" H 6600 3500 60  0000 C CNN
	1    6600 3500
	1    0    0    -1  
$EndComp
Text Notes 8450 3200 0    60   ~ 0
-pspice\n* \n.MODEL Q2N3904  NPN\n+ IS=4.9148E-15\n+ BF=191.70\n+ VAF=100\n+ IKF=.28579\n+ ISE=11.882E-15\n+ NE=1.4422\n+ BR=5.6808\n+ VAR=100\n+ IKR=61.753E-3\n+ ISC=71.145E-12\n+ NC=1.6595\n+ NK=.8296\n+ RB=5.8072\n+ RC=.70808\n+ CJE=6.9435E-12\n+ VJE=.61872\n+ MJE=.27802\n+ CJC=3.7572E-12\n+ VJC=1.2237\n+ MJC=.28886\n+ TF=523.89E-12\n+ XTF=83.066\n+ VTF=67.769\n+ ITF=1.8804\n+ TR=10.000E-9
Text Notes 7700 6050 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 10uS 50mS 0mS\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(mon)\n  + v(in)\n  + v(out)\n  + v(gnd_a)\n*  + v(gnd_b)\n.endc
$Comp
L QNPN Q1
U 1 1 550C9701
P 5900 3200
F 0 "Q1" H 5900 3400 60  0000 C CNN
F 1 "Q2N2222A" V 6150 3200 60  0000 C CNN
F 2 "" H 5575 3200 60  0000 C CNN
F 3 "" H 5575 3200 60  0000 C CNN
	1    5900 3200
	1    0    0    -1  
$EndComp
Text Notes 4250 1950 0    60   ~ 0
-pspice\n*\n* Model generated on Feb 28, 13\n* MODEL FORMAT: PSpice\n.MODEL Q2n2222a npn\n+IS=3.88184e-14 BF=929.846 NF=1.10496 VAF=16.5003\n+IKF=0.019539 ISE=1.0168e-11 NE=1.94752 BR=48.4545\n+NR=1.07004 VAR=40.538 IKR=0.19539 ISC=1.0168e-11\n+NC=4 RB=0.1 IRB=0.1 RBM=0.1\n+RE=0.0001 RC=0.426673 XTB=0.1 XTI=1\n+EG=1.05 CJE=2.23677e-11 VJE=0.582701 MJE=0.63466\n+TF=4.06711e-10 XTF=3.92912 VTF=17712.6 ITF=0.4334\n+CJC=2.23943e-11 VJC=0.576146 MJC=0.632796 XCJC=1\n+FC=0.170253 CJS=0 VJS=0.75 MJS=0.5\n+TR=1e-07 PTF=0 KF=0 AF=1
$Comp
L V V2
U 1 1 572C2687
P 4000 3500
F 0 "V2" V 3800 3500 60  0000 C CNN
F 1 "PWL 0mS 0V 10mS 0V 11mS 3.2V 19mS 3.2V 20mS 0V 30mS 0V" H 3650 3950 47  0000 C CNN
F 2 "" H 4000 3500 60  0000 C CNN
F 3 "" H 4000 3500 60  0000 C CNN
	1    4000 3500
	1    0    0    -1  
$EndComp
Text GLabel 6000 2900 1    60   Input ~ 0
VCC
$Comp
L R R2
U 1 1 572C2897
P 6000 3700
F 0 "R2" H 6000 3800 50  0000 C CNN
F 1 "100" H 6000 3600 50  0000 C CNN
F 2 "" V 5930 3700 30  0000 C CNN
F 3 "" H 6000 3700 30  0000 C CNN
	1    6000 3700
	0    -1   -1   0   
$EndComp
Text GLabel 5900 4000 0    60   Input ~ 0
GND_A
$Comp
L R R1
U 1 1 572C29B8
P 5400 3200
F 0 "R1" H 5400 3300 50  0000 C CNN
F 1 "1K" H 5400 3100 50  0000 C CNN
F 2 "" V 5330 3200 30  0000 C CNN
F 3 "" H 5400 3200 30  0000 C CNN
	1    5400 3200
	1    0    0    -1  
$EndComp
$Comp
L QNPN Q2
U 1 1 572C2A8B
P 6200 4600
F 0 "Q2" H 6200 4800 60  0000 C CNN
F 1 "Q2N2222A" V 6450 4600 60  0000 C CNN
F 2 "" H 5875 4600 60  0000 C CNN
F 3 "" H 5875 4600 60  0000 C CNN
	1    6200 4600
	1    0    0    -1  
$EndComp
Text GLabel 5900 4600 0    60   Input ~ 0
GND_B
Text GLabel 5200 4000 0    60   Input ~ 0
GND_A
$Comp
L R R3
U 1 1 572C2BEA
P 5300 4300
F 0 "R3" H 5300 4400 50  0000 C CNN
F 1 "180K" H 5300 4200 50  0000 C CNN
F 2 "" V 5230 4300 30  0000 C CNN
F 3 "" H 5300 4300 30  0000 C CNN
	1    5300 4300
	0    -1   -1   0   
$EndComp
Text GLabel 5200 4600 0    60   Input ~ 0
GND_B
Text GLabel 6300 4300 1    60   Input ~ 0
VCC
$Comp
L R R4
U 1 1 572C2EC0
P 6300 5200
F 0 "R4" H 6300 5300 50  0000 C CNN
F 1 "1K" H 6300 5100 50  0000 C CNN
F 2 "" V 6230 5200 30  0000 C CNN
F 3 "" H 6300 5200 30  0000 C CNN
	1    6300 5200
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND02
U 1 1 572C2F5D
P 6300 5500
F 0 "#GND02" H 6300 5400 40  0001 C CNN
F 1 "0" H 6300 5430 40  0000 C CNN
F 2 "" H 6300 5500 60  0000 C CNN
F 3 "" H 6300 5500 60  0000 C CNN
	1    6300 5500
	1    0    0    -1  
$EndComp
Text GLabel 6400 4900 2    60   Input ~ 0
OUT
Text GLabel 5100 3200 0    60   Input ~ 0
MON
Wire Wire Line
	6600 3400 6600 3500
Wire Wire Line
	6000 2900 6000 3000
Wire Wire Line
	6600 2900 6600 3000
Wire Wire Line
	6000 3400 6000 3500
Wire Wire Line
	6000 3900 6000 4000
Wire Wire Line
	5200 4000 5300 4000
Wire Wire Line
	5300 4000 5300 4100
Wire Wire Line
	5300 4500 5300 4600
Wire Wire Line
	5300 4600 5200 4600
Wire Wire Line
	6000 4600 5900 4600
Wire Wire Line
	6000 4000 5900 4000
Wire Wire Line
	6300 4300 6300 4400
Wire Wire Line
	6300 4800 6300 5000
Wire Wire Line
	6300 5400 6300 5500
Wire Wire Line
	6300 4900 6400 4900
Connection ~ 6300 4900
Wire Wire Line
	5700 3200 5600 3200
Wire Wire Line
	5200 3200 5100 3200
Wire Wire Line
	4000 3300 4000 3200
Wire Wire Line
	4000 3200 4100 3200
Text GLabel 4100 3200 2    60   Input ~ 0
MON
$Comp
L 0 #GND03
U 1 1 572C3390
P 4000 3800
F 0 "#GND03" H 4000 3700 40  0001 C CNN
F 1 "0" H 4000 3730 40  0000 C CNN
F 2 "" H 4000 3800 60  0000 C CNN
F 3 "" H 4000 3800 60  0000 C CNN
	1    4000 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3700 4000 3800
Text GLabel 5650 3200 1    60   Input ~ 0
CTL
Text GLabel 6000 3450 0    60   Input ~ 0
IN
$EndSCHEMATC
