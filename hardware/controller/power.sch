EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:lph7366
LIBS:clock
LIBS:handler
LIBS:radio
LIBS:esdprot
LIBS:sdcard
LIBS:iface
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +12V #PWR063
U 1 1 572E7CBB
P 2000 3200
F 0 "#PWR063" H 2000 3050 50  0001 C CNN
F 1 "+12V" H 2000 3340 50  0000 C CNN
F 2 "" H 2000 3200 60  0000 C CNN
F 3 "" H 2000 3200 60  0000 C CNN
	1    2000 3200
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 572E7CCF
P 1700 3350
F 0 "P3" H 1700 3500 50  0000 C CNN
F 1 "12V" V 1800 3350 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02" H 1700 3350 60  0001 C CNN
F 3 "" H 1700 3350 60  0000 C CNN
	1    1700 3350
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR064
U 1 1 572E7D14
P 2000 3500
F 0 "#PWR064" H 2000 3250 50  0001 C CNN
F 1 "GND" H 2000 3350 50  0000 C CNN
F 2 "" H 2000 3500 60  0000 C CNN
F 3 "" H 2000 3500 60  0000 C CNN
	1    2000 3500
	1    0    0    -1  
$EndComp
$Comp
L LD1117S33CTR U3
U 1 1 572E84AF
P 4300 2050
F 0 "U3" H 4300 2300 40  0000 C CNN
F 1 "LD1117S33CTR" H 4300 2250 40  0000 C CNN
F 2 "Local:SOT-223" H 4300 2150 40  0001 C CNN
F 3 "" H 4300 2050 60  0000 C CNN
	1    4300 2050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR065
U 1 1 572E84F8
P 3800 1900
F 0 "#PWR065" H 3800 1750 50  0001 C CNN
F 1 "+5V" H 3800 2040 50  0000 C CNN
F 2 "" H 3800 1900 60  0000 C CNN
F 3 "" H 3800 1900 60  0000 C CNN
	1    3800 1900
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR066
U 1 1 572E853A
P 4800 1900
F 0 "#PWR066" H 4800 1750 50  0001 C CNN
F 1 "+3V3" H 4800 2040 50  0000 C CNN
F 2 "" H 4800 1900 60  0000 C CNN
F 3 "" H 4800 1900 60  0000 C CNN
	1    4800 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR067
U 1 1 572E855B
P 4300 2400
F 0 "#PWR067" H 4300 2150 50  0001 C CNN
F 1 "GND" H 4300 2250 50  0000 C CNN
F 2 "" H 4300 2400 60  0000 C CNN
F 3 "" H 4300 2400 60  0000 C CNN
	1    4300 2400
	1    0    0    -1  
$EndComp
$Comp
L CP C24
U 1 1 572E93A6
P 4800 2250
F 0 "C24" H 4825 2350 50  0000 L CNN
F 1 "220u 6.3V" H 4600 2150 50  0000 L CNN
F 2 "Local:C_Radial_D8_L11.5_P3.5" H 4838 2100 30  0001 C CNN
F 3 "" H 4800 2250 60  0000 C CNN
	1    4800 2250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR068
U 1 1 572E9497
P 4800 2500
F 0 "#PWR068" H 4800 2250 50  0001 C CNN
F 1 "GND" H 4800 2350 50  0000 C CNN
F 2 "" H 4800 2500 60  0000 C CNN
F 3 "" H 4800 2500 60  0000 C CNN
	1    4800 2500
	1    0    0    -1  
$EndComp
$Comp
L C C25
U 1 1 57310B2E
P 5500 2250
F 0 "C25" H 5525 2350 50  0000 L CNN
F 1 "100n" H 5525 2150 50  0000 L CNN
F 2 "Local:C_0603" H 5538 2100 30  0001 C CNN
F 3 "" H 5500 2250 60  0000 C CNN
	1    5500 2250
	1    0    0    -1  
$EndComp
$Comp
L C C26
U 1 1 57310B35
P 5800 2250
F 0 "C26" H 5825 2350 50  0000 L CNN
F 1 "100n" H 5825 2150 50  0000 L CNN
F 2 "Local:C_0603" H 5838 2100 30  0001 C CNN
F 3 "" H 5800 2250 60  0000 C CNN
	1    5800 2250
	1    0    0    -1  
$EndComp
$Comp
L C C27
U 1 1 57310B3C
P 6100 2250
F 0 "C27" H 6125 2350 50  0000 L CNN
F 1 "100n" H 6125 2150 50  0000 L CNN
F 2 "Local:C_0603" H 6138 2100 30  0001 C CNN
F 3 "" H 6100 2250 60  0000 C CNN
	1    6100 2250
	1    0    0    -1  
$EndComp
$Comp
L C C28
U 1 1 57310B43
P 6400 2250
F 0 "C28" H 6425 2350 50  0000 L CNN
F 1 "100n" H 6425 2150 50  0000 L CNN
F 2 "Local:C_0603" H 6438 2100 30  0001 C CNN
F 3 "" H 6400 2250 60  0000 C CNN
	1    6400 2250
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L1
U 1 1 57310B4A
P 7350 2000
F 0 "L1" H 7350 2100 50  0000 C CNN
F 1 "420R" H 7350 1950 50  0000 C CNN
F 2 "Local:R_0603" H 7350 2000 60  0001 C CNN
F 3 "" H 7350 2000 60  0000 C CNN
	1    7350 2000
	1    0    0    -1  
$EndComp
$Comp
L C C29
U 1 1 57310B51
P 7700 2250
F 0 "C29" H 7725 2350 50  0000 L CNN
F 1 "10n" H 7725 2150 50  0000 L CNN
F 2 "Local:C_0603" H 7738 2100 30  0001 C CNN
F 3 "" H 7700 2250 60  0000 C CNN
	1    7700 2250
	1    0    0    -1  
$EndComp
Text GLabel 8000 1900 1    47   Input ~ 0
3V3A
$Comp
L GND #PWR069
U 1 1 57310B59
P 5950 2600
F 0 "#PWR069" H 5950 2350 50  0001 C CNN
F 1 "GND" H 5950 2450 50  0000 C CNN
F 2 "" H 5950 2600 60  0000 C CNN
F 3 "" H 5950 2600 60  0000 C CNN
	1    5950 2600
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR070
U 1 1 57310B5F
P 5950 1900
F 0 "#PWR070" H 5950 1750 50  0001 C CNN
F 1 "+3V3" H 5950 2040 50  0000 C CNN
F 2 "" H 5950 1900 60  0000 C CNN
F 3 "" H 5950 1900 60  0000 C CNN
	1    5950 1900
	1    0    0    -1  
$EndComp
$Comp
L CP C30
U 1 1 57310B65
P 8000 2250
F 0 "C30" H 8025 2350 50  0000 L CNN
F 1 "10u" H 8025 2150 50  0000 L CNN
F 2 "Local:C_0805" H 8038 2100 30  0001 C CNN
F 3 "" H 8000 2250 60  0000 C CNN
	1    8000 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3300 2000 3300
Wire Wire Line
	2000 3300 2000 3200
Wire Wire Line
	1900 3400 2000 3400
Wire Wire Line
	2000 3400 2000 3500
Wire Wire Line
	3900 2000 3800 2000
Wire Wire Line
	3800 2000 3800 1900
Wire Wire Line
	4700 2000 4800 2000
Wire Wire Line
	4800 1900 4800 2100
Wire Wire Line
	4300 2300 4300 2400
Connection ~ 4800 2000
Wire Wire Line
	4800 2400 4800 2500
Wire Wire Line
	8000 2500 8000 2400
Connection ~ 7700 2000
Wire Wire Line
	8000 1900 8000 2100
Wire Wire Line
	7700 2500 7700 2400
Wire Wire Line
	7700 2000 7700 2100
Wire Wire Line
	7600 2000 8000 2000
Connection ~ 5800 2500
Wire Wire Line
	5500 2500 5500 2400
Connection ~ 6100 2500
Wire Wire Line
	5800 2500 5800 2400
Wire Wire Line
	6100 2500 6100 2400
Wire Wire Line
	7700 2500 8000 2500
Wire Wire Line
	6400 2500 6400 2400
Connection ~ 5800 2000
Wire Wire Line
	5500 2000 5500 2100
Connection ~ 6100 2000
Wire Wire Line
	5800 2000 5800 2100
Wire Wire Line
	6100 2000 6100 2100
Wire Wire Line
	6400 2000 6400 2100
Wire Wire Line
	5500 2000 6400 2000
Connection ~ 8000 2000
Wire Wire Line
	5500 2500 6400 2500
$Comp
L +3V3 #PWR071
U 1 1 5732E5B9
P 7000 1900
F 0 "#PWR071" H 7000 1750 50  0001 C CNN
F 1 "+3V3" H 7000 2040 50  0000 C CNN
F 2 "" H 7000 1900 60  0000 C CNN
F 3 "" H 7000 1900 60  0000 C CNN
	1    7000 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2000 7000 2000
Wire Wire Line
	7000 2000 7000 1900
Wire Wire Line
	7850 2500 7850 2600
Connection ~ 7850 2500
$Comp
L GND #PWR072
U 1 1 5732E732
P 7850 2600
F 0 "#PWR072" H 7850 2350 50  0001 C CNN
F 1 "GND" H 7850 2450 50  0000 C CNN
F 2 "" H 7850 2600 60  0000 C CNN
F 3 "" H 7850 2600 60  0000 C CNN
	1    7850 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1900 5950 2000
Connection ~ 5950 2000
Wire Wire Line
	5950 2600 5950 2500
Connection ~ 5950 2500
$Comp
L INDUCTOR_SMALL L2
U 1 1 5732F292
P 8950 2000
F 0 "L2" H 8950 2100 50  0000 C CNN
F 1 "420R" H 8950 1950 50  0000 C CNN
F 2 "Local:R_0603" H 8950 2000 60  0001 C CNN
F 3 "" H 8950 2000 60  0000 C CNN
	1    8950 2000
	1    0    0    -1  
$EndComp
$Comp
L C C31
U 1 1 5732F298
P 9300 2250
F 0 "C31" H 9325 2350 50  0000 L CNN
F 1 "10n" H 9325 2150 50  0000 L CNN
F 2 "Local:C_0603" H 9338 2100 30  0001 C CNN
F 3 "" H 9300 2250 60  0000 C CNN
	1    9300 2250
	1    0    0    -1  
$EndComp
Text GLabel 9600 1900 1    47   Input ~ 0
3V3B
$Comp
L CP C32
U 1 1 5732F29F
P 9600 2250
F 0 "C32" H 9625 2350 50  0000 L CNN
F 1 "10u" H 9625 2150 50  0000 L CNN
F 2 "Local:C_0805" H 9638 2100 30  0001 C CNN
F 3 "" H 9600 2250 60  0000 C CNN
	1    9600 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 2500 9600 2400
Connection ~ 9300 2000
Wire Wire Line
	9600 1900 9600 2100
Wire Wire Line
	9300 2500 9300 2400
Wire Wire Line
	9300 2000 9300 2100
Wire Wire Line
	9200 2000 9600 2000
Wire Wire Line
	9300 2500 9600 2500
Connection ~ 9600 2000
$Comp
L +3V3 #PWR073
U 1 1 5732F2AD
P 8600 1900
F 0 "#PWR073" H 8600 1750 50  0001 C CNN
F 1 "+3V3" H 8600 2040 50  0000 C CNN
F 2 "" H 8600 1900 60  0000 C CNN
F 3 "" H 8600 1900 60  0000 C CNN
	1    8600 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 2000 8600 2000
Wire Wire Line
	8600 2000 8600 1900
Wire Wire Line
	9450 2500 9450 2600
Connection ~ 9450 2500
$Comp
L GND #PWR074
U 1 1 5732F2B7
P 9450 2600
F 0 "#PWR074" H 9450 2350 50  0001 C CNN
F 1 "GND" H 9450 2450 50  0000 C CNN
F 2 "" H 9450 2600 60  0000 C CNN
F 3 "" H 9450 2600 60  0000 C CNN
	1    9450 2600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P6
U 1 1 576936CD
P 3200 2050
F 0 "P6" H 3200 2200 50  0000 C CNN
F 1 "5V" V 3300 2050 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02" H 3200 2050 60  0001 C CNN
F 3 "" H 3200 2050 60  0000 C CNN
	1    3200 2050
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR075
U 1 1 576936D3
P 3500 2200
F 0 "#PWR075" H 3500 1950 50  0001 C CNN
F 1 "GND" H 3500 2050 50  0000 C CNN
F 2 "" H 3500 2200 60  0000 C CNN
F 3 "" H 3500 2200 60  0000 C CNN
	1    3500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2000 3500 2000
Wire Wire Line
	3500 2000 3500 1900
Wire Wire Line
	3400 2100 3500 2100
Wire Wire Line
	3500 2100 3500 2200
$Comp
L +5V #PWR076
U 1 1 5769377E
P 3500 1900
F 0 "#PWR076" H 3500 1750 50  0001 C CNN
F 1 "+5V" H 3500 2040 50  0000 C CNN
F 2 "" H 3500 1900 50  0000 C CNN
F 3 "" H 3500 1900 50  0000 C CNN
	1    3500 1900
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 576940EC
P 2500 3350
F 0 "C1" H 2525 3450 50  0000 L CNN
F 1 "100n" H 2525 3250 50  0000 L CNN
F 2 "Local:C_0603" H 2538 3200 30  0001 C CNN
F 3 "" H 2500 3350 60  0000 C CNN
	1    2500 3350
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 576943B8
P 2900 3350
F 0 "C2" H 2925 3450 50  0000 L CNN
F 1 "100n" H 2925 3250 50  0000 L CNN
F 2 "Local:C_0603" H 2938 3200 30  0001 C CNN
F 3 "" H 2900 3350 60  0000 C CNN
	1    2900 3350
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR077
U 1 1 57694418
P 2700 3000
F 0 "#PWR077" H 2700 2850 50  0001 C CNN
F 1 "+12V" H 2700 3140 50  0000 C CNN
F 2 "" H 2700 3000 60  0000 C CNN
F 3 "" H 2700 3000 60  0000 C CNN
	1    2700 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3200 2500 3100
Wire Wire Line
	2500 3100 2900 3100
Wire Wire Line
	2700 3100 2700 3000
Wire Wire Line
	2900 3100 2900 3200
Connection ~ 2700 3100
Wire Wire Line
	2500 3500 2500 3600
Wire Wire Line
	2500 3600 2900 3600
Wire Wire Line
	2900 3600 2900 3500
Wire Wire Line
	2700 3600 2700 3700
Connection ~ 2700 3600
$Comp
L GND #PWR078
U 1 1 576945E4
P 2700 3700
F 0 "#PWR078" H 2700 3450 50  0001 C CNN
F 1 "GND" H 2700 3550 50  0000 C CNN
F 2 "" H 2700 3700 60  0000 C CNN
F 3 "" H 2700 3700 60  0000 C CNN
	1    2700 3700
	1    0    0    -1  
$EndComp
$Comp
L HEATSINK HS1
U 1 1 5769817C
P 5200 4100
F 0 "HS1" H 5200 4300 50  0000 C CNN
F 1 "BOX" H 5200 4050 50  0000 C CNN
F 2 "Local:IP55_CAP" H 5200 4100 50  0001 C CNN
F 3 "" H 5200 4100 50  0000 C CNN
	1    5200 4100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 M1
U 1 1 576B0F7F
P 3400 4700
F 0 "M1" H 3400 4800 50  0000 C CNN
F 1 "MNT" V 3500 4700 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 3400 4700 50  0001 C CNN
F 3 "" H 3400 4700 50  0000 C CNN
	1    3400 4700
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 M2
U 1 1 576B10BB
P 3700 4700
F 0 "M2" H 3700 4800 50  0000 C CNN
F 1 "MNT" V 3800 4700 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 3700 4700 50  0001 C CNN
F 3 "" H 3700 4700 50  0000 C CNN
	1    3700 4700
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 M3
U 1 1 576B1132
P 4000 4700
F 0 "M3" H 4000 4800 50  0000 C CNN
F 1 "MNT" V 4100 4700 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 4000 4700 50  0001 C CNN
F 3 "" H 4000 4700 50  0000 C CNN
	1    4000 4700
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 M4
U 1 1 576B118A
P 4300 4700
F 0 "M4" H 4300 4800 50  0000 C CNN
F 1 "MNT" V 4400 4700 50  0000 C CNN
F 2 "Local:MountingHole_3mm" H 4300 4700 50  0001 C CNN
F 3 "" H 4300 4700 50  0000 C CNN
	1    4300 4700
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR079
U 1 1 576B11E1
P 3400 5000
F 0 "#PWR079" H 3400 4750 50  0001 C CNN
F 1 "GND" H 3400 4850 50  0000 C CNN
F 2 "" H 3400 5000 60  0000 C CNN
F 3 "" H 3400 5000 60  0000 C CNN
	1    3400 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR080
U 1 1 576B122B
P 3700 5000
F 0 "#PWR080" H 3700 4750 50  0001 C CNN
F 1 "GND" H 3700 4850 50  0000 C CNN
F 2 "" H 3700 5000 60  0000 C CNN
F 3 "" H 3700 5000 60  0000 C CNN
	1    3700 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR081
U 1 1 576B1275
P 4000 5000
F 0 "#PWR081" H 4000 4750 50  0001 C CNN
F 1 "GND" H 4000 4850 50  0000 C CNN
F 2 "" H 4000 5000 60  0000 C CNN
F 3 "" H 4000 5000 60  0000 C CNN
	1    4000 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR082
U 1 1 576B12BF
P 4300 5000
F 0 "#PWR082" H 4300 4750 50  0001 C CNN
F 1 "GND" H 4300 4850 50  0000 C CNN
F 2 "" H 4300 5000 60  0000 C CNN
F 3 "" H 4300 5000 60  0000 C CNN
	1    4300 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5000 4300 4900
Wire Wire Line
	4000 5000 4000 4900
Wire Wire Line
	3700 5000 3700 4900
Wire Wire Line
	3400 5000 3400 4900
$EndSCHEMATC
