#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

#include "macro.h"
#include "config.h"

void valve_init(void) {
  /* Enable TIM1 for backlight control */
  rcc_periph_clock_enable(RCC_TIM1);
  
  timer_reset(TIM1);
  
  /* Timer global mode:
	 * - No divider
	 * - Alignment edge
	 * - Direction up
	 */
	timer_set_mode(TIM1,
                 TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE,
                 TIM_CR1_DIR_UP);

	timer_set_prescaler(TIM1, 0);
	timer_set_repetition_counter(TIM1, 0);
	timer_enable_preload(TIM1);
	timer_continuous_mode(TIM1);

#if 0
  /* Configure break and deadtime */
	timer_set_deadtime(TIM1, 0);
	timer_set_enabled_off_state_in_idle_mode(TIM1);
	timer_set_enabled_off_state_in_run_mode(TIM1);
	timer_disable_break(TIM1);
	timer_set_break_polarity_high(TIM1);
	timer_disable_break_automatic_output(TIM1);
	timer_set_break_lock(TIM1, TIM_BDTR_LOCK_OFF);
#endif

	timer_set_period(TIM1, 1<<12);
  timer_enable_break_main_output(TIM1);
  timer_enable_counter(TIM1);
}

void valve_done(void) {
  
}
