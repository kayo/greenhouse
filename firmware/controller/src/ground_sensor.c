#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include "proto.h"

#include "macro.h"
#include "config.h"
#include "ground_sensor.h"

#ifdef ground_sensor_rs485_de
#define RS485_DE_PORT _CAT2(GPIO, _NTH0(ground_sensor_rs485_de))
#define RS485_DE_PAD _CAT2(GPIO, _NTH1(ground_sensor_rs485_de))
#if _NTH2(ground_sensor_rs485_de)
#define rs485_de_enable() gpio_set(RS485_DE_PORT, RS485_DE_PAD)
#define rs485_de_disable() gpio_clear(RS485_DE_PORT, RS485_DE_PAD)
#else
#define rs485_de_enable() gpio_clear(RS485_DE_PORT, RS485_DE_PAD)
#define rs485_de_disable() gpio_set(RS485_DE_PORT, RS485_DE_PAD)
#endif
#else /* ground_sensor_rs485_de */
#define rs485_de_enable()
#define rs485_de_disable()
#endif /* ground_sensor_rs485_de */

#ifdef ground_sensor_rs485_re
#define RS485_RE_PORT _CAT2(GPIO, _NTH0(ground_sensor_rs485_re))
#define RS485_RE_PAD _CAT2(GPIO, _NTH1(ground_sensor_rs485_re))
#if _NTH2(ground_sensor_rs485_re)
#define rs485_re_enable() gpio_set(RS485_RE_PORT, RS485_RE_PAD)
#define rs485_re_disable() gpio_clear(RS485_RE_PORT, RS485_RE_PAD)
#else
#define rs485_re_enable() gpio_clear(RS485_RE_PORT, RS485_RE_PAD)
#define rs485_re_disable() gpio_set(RS485_RE_PORT, RS485_RE_PAD)
#endif
#else /* ground_sensor_rs485_re */
#define rs485_de_enable()
#define rs485_de_disable()
#endif /* ground_sensor_rs485_re */

#define UART_REMAP _CAT3(uart, _NTH0(ground_sensor_uart), _remap)
  
#define UART_TX_PORT _CAT4(GPIO_BANK_USART, _NTH0(ground_sensor_uart), UART_REMAP, _TX)
#define UART_TX_PAD _CAT4(GPIO_USART, _NTH0(ground_sensor_uart), UART_REMAP, _TX)

#define UART_RX_PORT _CAT4(GPIO_BANK_USART, _NTH0(ground_sensor_uart), UART_REMAP, _RX)
#define UART_RX_PAD _CAT4(GPIO_USART, _NTH0(ground_sensor_uart), UART_REMAP, _RX)

#define UART_RCC _CAT2(RCC_USART, _NTH0(ground_sensor_uart))
#define UART_DEV _CAT2(USART, _NTH0(ground_sensor_uart))
#define UART_BAUD _NTH1(ground_sensor_uart)
#define UART_BITS _NTH2(ground_sensor_uart)

#define UART_IRQ _CAT3(NVIC_USART, _NTH0(ground_sensor_uart), _IRQ)
#define uart_isr _CAT3(usart, _NTH0(ground_sensor_uart), _isr)

#define DMA_DEV _CAT2(DMA, _NTH0(ground_sensor_dma))
#define DMA_TX_CH _CAT2(DMA_CHANNEL, _NTH1(ground_sensor_dma))
#define DMA_RX_CH _CAT2(DMA_CHANNEL, _NTH2(ground_sensor_dma))
#define DMA_TX_IRQ _CAT5(NVIC_DMA, _NTH0(ground_sensor_dma), _CHANNEL, _NTH1(ground_sensor_dma), _IRQ)
#define dma_tx_isr _CAT5(dma, _NTH0(ground_sensor_dma), _channel, _NTH1(ground_sensor_dma), _isr)
#define DMA_BUF _NTH3(ground_sensor_dma)

#ifdef NVIC_DMA1_CHANNEL2_3_IRQ
#define dma1_channel2_isr dma1_channel2_3_isr
#define dma1_channel3_isr dma1_channel2_3_isr
#define NVIC_DMA1_CHANNEL2_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#define NVIC_DMA1_CHANNEL3_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#endif

#ifdef NVIC_DMA1_CHANNEL4_5_IRQ
#define dma1_channel4_isr dma1_channel4_5_isr
#define dma1_channel5_isr dma1_channel4_5_isr
#define NVIC_DMA1_CHANNEL4_IRQ NVIC_DMA1_CHANNEL4_5_IRQ
#define NVIC_DMA1_CHANNEL5_IRQ NVIC_DMA1_CHANNEL4_5_IRQ
#endif

#ifdef STM32F0
#define USART_STOPBITS_1 USART_CR2_STOP_1_0BIT
#define USART_SR_IDLE USART_ISR_IDLE
#define USART_SR_NE USART_ISR_RXNE
#define USART_SR_FE USART_ISR_FE
#define USART_SR_ORE USART_ISR_ORE
#endif

#ifdef STM32F1
#define USART_TDR USART_DR
#define USART_RDR USART_DR
#endif

static uint8_t buffer[DMA_BUF]; /* i/o data buffer */

static void tx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  
  rs485_de_disable();
}

static void tx_enable(uint16_t size) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_TX_CH, size);
  
  rs485_de_enable();

  USART_SR(UART_DEV) &= ~USART_SR_TC;
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_TX_CH);
}

static void rx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_RX_CH);

  rs485_re_disable();
}

static void rx_enable(void) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_RX_CH, sizeof(buffer));
  
  rs485_re_enable();
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_RX_CH);
}

void ground_sensor_init(void) {  
  rcc_periph_clock_enable(UART_RCC);
  
  nvic_enable_irq(DMA_TX_IRQ);
  nvic_enable_irq(UART_IRQ);

  /* configure port */
  gpio_set_mode(UART_TX_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, UART_TX_PAD);
  gpio_set_mode(UART_RX_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, UART_RX_PAD);

  rs485_de_disable();
  rs485_re_disable();
#ifdef RS485_DE_PORT
  gpio_set_mode(RS485_DE_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, RS485_DE_PAD);
#endif
#ifdef RS485_RE_PORT
  gpio_set_mode(RS485_RE_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, RS485_RE_PAD);
#endif

  /* configure uart */
  usart_set_baudrate(UART_DEV, UART_BAUD);
  usart_set_databits(UART_DEV, UART_BITS);
  usart_set_parity(UART_DEV, USART_PARITY_NONE);
  usart_set_stopbits(UART_DEV, USART_STOPBITS_1);
  usart_set_mode(UART_DEV, USART_MODE_TX_RX);
  usart_set_flow_control(UART_DEV, USART_FLOWCONTROL_NONE);

  /* rs485 */
#ifdef STM32F0
  USART_CR3(UART_DEV) |= ~USART_CR3_DEP | USART_CR3_DEM;
#endif

  /* TX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_TX_CH);
  dma_set_priority(DMA_DEV, DMA_TX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_TX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_TX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_TX_CH);
  dma_set_read_from_memory(DMA_DEV, DMA_TX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_TX_CH, (uint32_t)&USART_TDR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_TX_CH, (uint32_t)buffer);
  
  /* RX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_RX_CH);
  dma_set_priority(DMA_DEV, DMA_RX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_RX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_RX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_RX_CH);
  dma_enable_circular_mode(DMA_DEV, DMA_RX_CH);
  dma_set_read_from_peripheral(DMA_DEV, DMA_RX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_RX_CH, (uint32_t)&USART_RDR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_RX_CH, (uint32_t)buffer);
  
  /* Enable TX transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);

  /* Enable TX transfer error interrupt */
  /*dma_enable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  
  usart_enable_tx_dma(UART_DEV);
  usart_enable_rx_dma(UART_DEV);
  
  usart_enable(UART_DEV);
}

void ground_sensor_done(void) {
  usart_disable(UART_DEV);
  
  usart_disable_tx_dma(UART_DEV);
  usart_disable_rx_dma(UART_DEV);
  
  dma_disable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);

  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  dma_disable_channel(DMA_DEV, DMA_RX_CH);

  nvic_disable_irq(UART_IRQ);
  nvic_disable_irq(DMA_TX_IRQ);

  rcc_periph_clock_disable(UART_RCC);
}

static inline void usart_enable_tc_interrupt(uint32_t usart) {
  USART_CR1(usart) |= USART_CR1_TCIE;
}

static inline void usart_disable_tc_interrupt(uint32_t usart) {
  USART_CR1(usart) &= ~USART_CR1_TCIE;
}

void uart_isr(void) {
  usart_disable_tc_interrupt(UART_DEV);
  tx_disable();
  rx_enable();
}

void dma_tx_isr(void) {
  /*bool error = dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TEIF);*/
  dma_clear_interrupt_flags(DMA_DEV, DMA_TX_CH, DMA_GIF | DMA_TEIF | DMA_TCIF);
  usart_enable_tc_interrupt(UART_DEV);
}

static inline uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

static inline uint32_t dma_count_data(uint32_t dma, uint8_t channel) {
  return DMA_CNDTR(dma, channel);
}

/* current state of operation */
ground_sensor_state_t
ground_sensor_state =
  ground_sensor_idle;

void ground_sensor_step(void) {
  uint16_t length;
  
  if (!dma_is_active(DMA_DEV, DMA_RX_CH) ||
      !usart_get_flag(UART_DEV,
                      USART_SR_IDLE)) {
#if 0
    delay_t time = delay_snap();
    if (time - last_poll > POLL_INTERVAL) {
      last_poll = time;
      
      proto_req_t *req = proto_req_ptr(buffer);
      
      req->id = 0;
      req->op = proto_get_val;
      
      proto_set_req(buffer, &length);
      
      goto resume;
    }
#endif
    return;
  }

  if (usart_get_flag(UART_DEV, 
                     USART_SR_NE |
                     USART_SR_FE |
                     USART_SR_ORE)) {
    (void)usart_recv(UART_DEV);
    length = 0;
    goto resume;
  }
  
  length = sizeof(buffer) - dma_count_data(DMA_DEV, DMA_RX_CH);
  const proto_res_t *res = proto_res_ptr(buffer);

  if (!proto_get_res(buffer, &length) ||
      proto_success != res->st) {
    length = 0;

    switch (ground_sensor_state) {
    case ground_sensor_set_uin_pend:
      ground_sensor_state = ground_sensor_set_uin_fail;
      break;
    case ground_sensor_get_val_pend:
      ground_sensor_state = ground_sensor_get_val_fail;
      break;
    default:
      break;
    }
    
    goto resume;
  }

  switch (ground_sensor_state) {
  case ground_sensor_set_uin_pend:
    if (res->op == proto_set_opt) {
      ground_sensor_state = ground_sensor_set_uin_done;
    } else {
      ground_sensor_state = ground_sensor_set_uin_fail;
    }
    break;
  case ground_sensor_get_val_pend:
    if (res->op == proto_get_val) {
      ground_sensor_state = ground_sensor_get_val_done;
    } else {
      ground_sensor_state = ground_sensor_get_val_fail;
    }
    break;
  default:
    ground_sensor_state = ground_sensor_idle;
  }
  
 resume:
  rx_disable();
  
  if (length) {
    tx_enable(length);
  }
}

void ground_sensor_set_uin(uint8_t uin) {
  ground_sensor_state = ground_sensor_set_uin_pend;
  
  proto_req_t *req = proto_req_ptr(buffer);
  
  req->id = uin;
  req->op = proto_set_opt;

  uint16_t length;
  proto_set_req(buffer, &length);
  
  tx_enable(length);
}

void ground_sensor_get_val(uint8_t uin) {
  ground_sensor_state = ground_sensor_get_val_pend;
  
  proto_req_t *req = proto_req_ptr(buffer);
  
  req->id = uin;
  req->op = proto_get_val;

  uint16_t length;
  proto_set_req(buffer, &length);
  
  tx_enable(length);
}

void ground_sensor_ret_val(ground_sensor_value_t *val) {
  if (ground_sensor_state == ground_sensor_get_val_done) {
    const proto_res_t *res = proto_res_ptr(buffer);

    val->photo = res->val.photo;
    val->therm = res->val.therm;
    val->humid = res->val.humid;
    
    ground_sensor_state = ground_sensor_idle;
  }
}
