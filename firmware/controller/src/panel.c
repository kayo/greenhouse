#include "panel.h"

#include "pcd8544.h"
#include "bitmap.h"
#include "ioport.h"
#include "encoder.h"
#include "button.h"

#include <stdio.h>

#include "macro.h"
#include "config.h"

#define IO_PORT_A 0
#define IO_PORT_B 1
#define IO_PORT_C 2
#define IO_PORT_D 3

/* IO ports definition for handler */
io_port_def(GPIOA, GPIOB, GPIOC, GPIOD);

#define ENCODER_INCR enc_ev(0, 0)
#define ENCODER_DECR enc_ev(0, 1)

enc_in_def(encoder) {
  { /* ENCODER_INCR */ io_node(_CAT2(IO_PORT_, _NTH0(hndenc_a)), _NTH1(hndenc_b)),
    /* ENCODER_DECR */ io_node(_CAT2(IO_PORT_, _NTH0(hndenc_b)), _NTH1(hndenc_b)) }
};
enc_drv_def(encoder, 0);

#define BUTTON_ENTER btn_ev(0, 0)
#define BUTTON_RESET btn_ev(0, 1)

btn_ev_def(button) { /* BUTTON_ENTER */ 100, /* BUTTON_RESET */ 500 };
btn_in_def(button) { /* BUTTON */ io_node(_CAT2(IO_PORT_, _NTH0(hndenc_e)), _NTH1(hndenc_e)) };
btn_drv_def(button, 0);

static bitmap_state_t canvas_state;

static const bitmap_canvas_t canvas = {
  pcd8544_frame_buffer,
  { {{ _NTH0(bitmap_config), _NTH1(bitmap_config) }},
    {{ _NTH2(bitmap_config), _NTH3(bitmap_config) }} },
  &canvas_state
};

void panel_init(void) {
  enc_init(&encoder);
  btn_init(&button);

  pcd8544_init();

  pcd8544_backlight(25);
  
  bitmap_init(&canvas);
  
  bitmap_font_def(font_8x8);
  bitmap_font(&canvas, &font_8x8);
  
  bitmap_operation(&canvas, bitmap_clear);
  bitmap_apply(&canvas);
  
  bitmap_operation(&canvas, bitmap_draw);
  bitmap_primitive(&canvas, bitmap_line);
  
  bitmap_align(&canvas, bitmap_top | bitmap_left);

  //bitmap_text(&canvas, 0, 0, "Иниц-я");
  bitmap_text(&canvas, 8, 8, "Иниц-я");
  pcd8544_update();
}

void panel_done(void) {
  enc_done(&encoder);
  btn_done(&button);
  pcd8544_done();
}

void panel_step(void) {
  enc_update(&encoder);
  btn_update(&button, _NTH1(systick_config));
  
  /*
  bitmap_operation(&canvas, bitmap_clear);
  bitmap_apply(&canvas);
  bitmap_operation(&canvas, bitmap_draw);
  */
  {
    char str[32];
    int16_t value = 0;
    
    //snprintf(str, sizeof(str), "Влажн.: %d", value);
    //bitmap_text(&canvas, 0, 0, str);
    /*
    snprintf(str, sizeof(str), "Темп.: %d", value);
    bitmap_text(&canvas, 0, 10, str);
    
    snprintf(str, sizeof(str), "Освещ.: %d", value);
    bitmap_text(&canvas, 0, 20, str);
    */
  }
  
  //pcd8544_update();
}
