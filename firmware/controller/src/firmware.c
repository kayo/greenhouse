#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

#include "macro.h"
#include "systick.h"
#include "monitor.h"
#include "ds3231.h"
#include "ground_sensor.h"
#include "panel.h"
#include "config.h"
#include "delay.h"
#include "pwm.h"
#include "crc.h"
#include "i2c.h"
#include "valve.h"

monitor_def();

pwm_def(1);
pwm_def(2);
i2c_def(1);

volatile enum {
  event_nothing = 0x0,
  event_realtime = 0x1,
} event;

static inline void hardware_init(void) {
  /* Enable GPIO clock */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOC);
  
  /* Enable AFIO clock */
  rcc_periph_clock_enable(RCC_AFIO);
  /* Setup remapping */
  gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON,
                     AFIO_MAPR_TIM3_REMAP_FULL_REMAP);
  
  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA1);

  /* RTC EXTI */
  nvic_enable_irq(NVIC_EXTI15_10_IRQ);
}

static inline void hardware_done(void) {
  /* RTC EXTI */
  nvic_disable_irq(NVIC_EXTI15_10_IRQ);
  
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA1);

  /* Disable AFIO clock */
  rcc_periph_clock_disable(RCC_AFIO);

  /* Disable GPIO clock */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOB);
  rcc_periph_clock_disable(RCC_GPIOC);
}

void exti15_10_isr(void) {
  event |= event_realtime;
}

static inline void init(void) { 
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  
  delay_init();
  delay_ms(300);
  hardware_init();
  crc32_init();
  pwm1_init();
  pwm2_init();
  i2c1_init();
  systick_init();
  //delay_ms(300);
  ds3231_init();
  ground_sensor_init();
  panel_init();
}

#if 0
static inline void done(void) {
  panel_done();
  ground_sensor_done();
  ds3231_done();
  systick_done();
  i2c1_done();
  pwm1_done();
  pwm2_done();
  hardware_done();
}
#else
#define done()
#endif

ds3231_time_t real_time;

ground_sensor_value_t ground_sensor1;

int main(void) {
  init();
  
  ds3231_read(&real_time);
  monitor_init();

  //pwm2_set3(1<<11);
  //pwm2_set3(3000);
  pwm2_set3((1<<12)-1);
  
  for (;;) {
    event = event_nothing;
    monitor_wait();

    if (event & event_realtime) {
      //realtime_update();
      ds3231_read(&real_time);
    }

    if (ground_sensor_state == ground_sensor_idle) {
      ground_sensor_get_val(0);
    }
    
    panel_step();
    ground_sensor_step();

    if (ground_sensor_state == ground_sensor_get_val_done) {
      ground_sensor_ret_val(&ground_sensor1);
    }
  }
  
  done();
  
  return 0;
}
