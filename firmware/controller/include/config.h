/* common config */
#define mcu_frequency 72
#define systick_config 8,100

/* pcd8544 config */
#define pcd8544_spi 2,32 /* device,divider */
#define pcd8544_dma 1,5 /* controller,channel */
#define pcd8544_dc B,14 /* port,pad */
#define pcd8544_res C,7 /* port,pad */
#define pcd8544_bl_pwm 1,1 /* timer,channel */
//#define pcd8544_bl A,8 /* port,pad */
#define bitmap_config 84,46,0,0 /* left,top,right,bottom */

/* encoder handler */
#define hndenc_a B,0,0 /* port,pad,active level */
#define hndenc_b B,1,0 /* port,pad,active level */
#define hndenc_e B,2,0 /* port,pad,active level */

/* on-board analog sensors */
#define sensor_adc 1,12,1.2,3.3 /* device,bits,Vref,Vtop */
#define sensor_dma 1,1 /* device,channel */
#define photo_input C,1,11 /* port,pad,channel */
#define therm_input C,0,10 /* port,pad,channel */

/* RTC device */
#define ds3231_i2c 1 /* device */
#define ds3231_int C,14,sqw,1hz /* interrupt port,pad,config,options */

/* rs485 ground sensors */
#define ground_sensor_uart 2,19200,8 /* device,baud,bits */
#define ground_sensor_dma 1,7,6,256 /* device,tx_channel,rx_channel,buffer */
#define ground_sensor_rs485_de A,1,1 /* drive enable port,pad,active level */
#define ground_sensor_rs485_re A,0,0 /* receive enable port,pad,active level */
#define ground_sensor_poll 1000 /* polling delay (mS) */

/* interfaces */
//#define i2c1_config 1,std,36,DIV2, /* device,mode,frequency,duty,remap */
#define i2c1_config std,B,7,B,6

/* pcd8544 backlight pwm */
#define pwm1_config 1,0,1<<12,(pp,nc,nc,nc), /* timer,prescaler,period,(channels configs),remap */

/* valve control pwm */
#define pwm2_config 3,0,1<<12,(nc,nc,pp,pp),_FR /* timer,prescaler,period,(channels configs),remap */

/* remapping declarations */
#define spi2_remap
#define tim1_remap
#define uart2_remap
