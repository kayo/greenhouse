#ifndef __GROUND_SENSOR__
#define __GROUND_SENSOR__ "ground_sensor.h"

void ground_sensor_init(void);
void ground_sensor_done(void);

void ground_sensor_step(void);

typedef enum {
  ground_sensor_idle,
  ground_sensor_set_uin_pend,
  ground_sensor_set_uin_done,
  ground_sensor_set_uin_fail,
  ground_sensor_get_val_pend,
  ground_sensor_get_val_done,
  ground_sensor_get_val_fail,
} ground_sensor_state_t;

extern ground_sensor_state_t ground_sensor_state;

typedef struct {
  int16_t photo;
  int16_t therm;
  int16_t humid;
} ground_sensor_value_t;

void ground_sensor_set_uin(uint8_t uin);
void ground_sensor_get_val(uint8_t uin);
void ground_sensor_ret_val(ground_sensor_value_t *val);

#endif /* __GROUND_SENSOR__ */
