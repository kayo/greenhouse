#ifndef __VALVE_H__
#define __VALVE_H__ "valve.h"

void valve_init(void);
void valve_done(void);

void valve_set(uint8_t index, uint8_t state);

#endif /* __VALVE_H__ */
