#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>

#include <stddef.h>

#include "macro.h"
#include "config.h"
#include "i2c.h"

i2c_def(_NTH0(ds3231_i2c));

#define i2c_io _CAT3(i2c, _NTH0(ds3231_i2c), _io)

#ifdef ds3231_int
#define INT_PORT _CAT2(GPIO, _NTH0(ds3231_int))
#define INT_PAD _CAT2(GPIO, _NTH1(ds3231_int))
#define INT_EXTI _CAT2(EXTI, _NTH1(ds3231_int))

#define INT_CNF_sqw 0
#define INT_CNF_alm 1

#define INT_CNF _CAT2(INT_CNF_, _NTH2(ds3231))
#endif

#include "ds3231_def.h"
#include "delay.h"

#include "ds3231.h"

void ds3231_read(ds3231_time_t *time) {
  ds3231_time_reg_t time_reg;

  const uint8_t time_reg_addr[] = {DS3231_REG_TIMEVAL};

  i2c_io(DS3231_I2C_ADDRESS,
         time_reg_addr, sizeof(time_reg_addr),
         (uint8_t*)&time_reg, sizeof(time_reg));
  
  time->year = ds3231_from_reg(time_reg.year) + 2000;
  time->month = ds3231_from_reg(time_reg.month);
  time->date = ds3231_from_reg(time_reg.date);
  
  time->day = time_reg.day;
  
  time->hour = ds3231_from_reg_24h(time_reg.hours);
  time->minute = ds3231_from_reg(time_reg.minutes);
  time->second = ds3231_from_reg(time_reg.seconds);
}

void ds3231_write_field(uint8_t field, uint16_t value) {
  struct {
    uint8_t dst;
    ds3231_reg_t reg;
  } t;
  
  switch (field) {
  case ds3231_field(year):
    t.reg = ds3231_to_reg(value - 2000);
    t.dst = DS3231_REG_YEAR;
    break;
  case ds3231_field(month):
    t.reg = ds3231_to_reg(value);
    t.dst = DS3231_REG_MONTH;
    break;
  case ds3231_field(date):
    t.reg = ds3231_to_reg(value);
    t.dst = DS3231_REG_DATE;
    break;
  case ds3231_field(day):
    t.reg = ds3231_to_reg(value);
    t.dst = DS3231_REG_WEEKDAY;
    break;
  case ds3231_field(hour):
    t.reg = ds3231_to_reg_24h(value);
    t.dst = DS3231_REG_HOURS;
    break;
  case ds3231_field(minute):
    t.reg = ds3231_to_reg(value);
    t.dst = DS3231_REG_MINUTES;
    break;
  case ds3231_field(second):
    t.reg = ds3231_to_reg(value);
    t.dst = DS3231_REG_SECONDS;
    break;
  default:
    return;
  }
  
  i2c_io(DS3231_I2C_ADDRESS, (const uint8_t*)&t, sizeof(t), NULL, 0);
}

void ds3231_init(void) {
#ifdef INT_PORT
  /* Setup external inperrupt input */
  gpio_set_mode(INT_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                INT_PAD);
  /* Setup pullup */
  gpio_set(INT_PORT, INT_PAD);

  /* Setup external interrupt */
  exti_select_source(INT_EXTI, INT_PORT);
  exti_set_trigger(INT_EXTI, EXTI_TRIGGER_FALLING);
  exti_enable_request(INT_EXTI);
#endif
  
  /* Touch rtc device */
  i2c_io(DS3231_I2C_ADDRESS, NULL, 0, NULL, 0);

#ifdef INT_CNF
  {
    /* Configure interrupt */
    struct {
      uint8_t addr;
      ds3231_reg_t reg;
    } data = {
      DS3231_REG_CONTROL,
      0,
    };
    
    i2c_io(DS3231_I2C_ADDRESS,
           (const uint8_t*)&data.addr, sizeof(data.addr),
           (uint8_t*)&data.reg, sizeof(data.reg));

    data.reg &= ~(DS3231_CONTROL_SQW | DS3231_CONTROL_INTCN);
    data.reg |= DS3231_CONTROL_SQW_1HZ | DS3231_CONTROL_INTCN_SQW;
    
    i2c_io(DS3231_I2C_ADDRESS,
           (const uint8_t*)&data, sizeof(data),
           NULL, 0);
  }
#endif
}
