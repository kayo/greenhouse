libds3231.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libds3231
libds3231.INHERIT := firmware libopencm3 libsystem
libds3231.CDIRS := $(libds3231.BASEPATH)include
libds3231.SRCS := $(addprefix $(libds3231.BASEPATH)src/,\
  ds3231.c)
