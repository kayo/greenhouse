/**
 * @brief 7-bit i2c slave address
 */
#define DS3231_I2C_ADDRESS 0x68

/**
 * @brief Ram registers address
 */
#define DS3231_REG_ADDRESS 0x00

/**
 * @brief Time keeping register address
 */
#define DS3231_REG_TIMEVAL 0x00

/**
 * @brief Seconds register address
 */
#define DS3231_REG_SECONDS 0x00

/**
 * @brief Minutes register address
 */
#define DS3231_REG_MINUTES 0x01

/**
 * @brief Hours register address
 */
#define DS3231_REG_HOURS   0x02

/**
 * @brief Day of week register address
 */
#define DS3231_REG_WEEKDAY 0x03

/**
 * @brief Date register address
 */
#define DS3231_REG_DATE    0x04

/**
 * @brief Month register address
 */
#define DS3231_REG_MONTH   0x05

/**
 * @brief Year register address
 */
#define DS3231_REG_YEAR    0x06

/**
 * @brief Alarm 1 register address
 */
#define DS3231_REG_ALARM_1 0x07

/**
 * @brief Alarm 2 register address
 */
#define DS3231_REG_ALARM_2 0x0b

/**
 * @brief Control register address
 */
#define DS3231_REG_CONTROL 0x0e

/**
 * @brief Control/Status register address
 */
#define DS3231_REG_CTLSTAT 0x0f

/**
 * @brief Aging offset register address
 */
#define DS3231_REG_AGINGOF 0x10

/**
 * @brief Temperature register
 */
#define DS3231_REG_TEMPVAL 0x11

/**
 * @brief 12/~24 hours format BIT
 */
#define DS3231_HOURS_12 (1 << 6)

/**
 * @brief ~AM/PM hours BIT
 */
#define DS3231_HOURS_PM (1 << 5)

/**
 * @brief Enable Oscillator control BIT
 */
#define DS3231_CONTROL_NEOSC (1 << 7)

/**
 * @brief Enable Battery-Backed Square-Wave control BIT
 */
#define DS3231_CONTROL_NEBBSQW (1 << 6)

/**
 * @brief Enable Temperature Conversion control BIT
 */
#define DS3231_CONTROL_CONVTEMP (1 << 5)

/**
 * @brief Square wave frequency mask
 */
#define DS3231_CONTROL_SQW (3 << 3)

/**
 * @brief Square wave frequency 1 Hz
 */
#define DS3231_CONTROL_SQW_1HZ (0 << 3)

/**
 * @brief Square wave frequency 1.024 kHz
 */
#define DS3231_CONTROL_SQW_1K024HZ (1 << 3)

/**
 * @brief Square wave frequency 4.096 kHz
 */
#define DS3231_CONTROL_SQW_4K096HZ (2 << 3)

/**
 * @brief Square wave frequency 8.192 kHz
 */
#define DS3231_CONTROL_SQW_8K192HZ (3 << 3)

/**
 * @brief Interrupt control BIT
 */
#define DS3231_CONTROL_INTCN (1 << 2)

/**
 * @brief Interrupt on Alarm
 */
#define DS3231_CONTROL_INTCW_ALM (1 << 2)

/**
 * @brief Interrupt on Square wave
 */
#define DS3231_CONTROL_INTCN_SQW (0 << 2)

/**
 * @brief Enable Alarm 2 Interrupt BIT
 */
#define DS3231_CONTROL_ALM2IE (1 << 1)

/**
 * @brief Enable Alarm 1 Interrupt BIT
 */
#define DS3231_CONTROL_ALM1IE (1 << 0)

/**
 * @brief Oscillator stop flag BIT
 */
#define DS3231_CTLSTAT_OSCSTOP (1 << 7)

/**
 * @brief Enable 32.768 kHz output BIT
 */
#define DS3231_CTLSTAT_EN32KHZ (1 << 3)

/**
 * @brief Busy flag BIT
 */
#define DS3231_CTLSTAT_BUZY (1 << 2)

/**
 * @brief Alarm 2 flag BIT
 */
#define DS3231_CTLSTAT_ALM2 (1 << 1)

/**
 * @brief Alarm 1 flag BIT
 */
#define DS3231_CTLSTAT_ALM1 (1 << 0)

/**
 * @brief Time register type
 */
typedef uint8_t ds3231_reg_t;

/**
 * @brief The time keeping registers
 */
typedef struct {
  ds3231_reg_t seconds;
  ds3231_reg_t minutes;
  ds3231_reg_t hours;
  ds3231_reg_t day;
  ds3231_reg_t date;
  ds3231_reg_t month;
  ds3231_reg_t year;
} ds3231_time_reg_t;

/**
 * @brief The alarm 1 registers
 */
typedef struct {
  ds3231_reg_t secconds;
  ds3231_reg_t minutes;
  ds3231_reg_t hours;
  ds3231_reg_t day;
  ds3231_reg_t date;
} ds3231_alm1_reg_t;

/**
 * @brief The alarm 2 registers
 */
typedef struct {
  ds3231_reg_t minutes;
  ds3231_reg_t hours;
  ds3231_reg_t day;
  ds3231_reg_t date;
} ds3231_alm2_reg_t;

/**
 * @brief The temperature registers
 */
typedef struct {
  ds3231_reg_t msb;
  ds3231_reg_t lsb;
} ds3231_temp_reg_t;

/**
 * @brief The all RAM registers
 */
typedef struct {
  ds3231_time_reg_t timeval;
  ds3231_alm1_reg_t alarm_1;
  ds3231_alm2_reg_t alarm_2;
  
  ds3231_reg_t control;
  ds3231_reg_t ctlstat;
  ds3231_reg_t agingof;
  
  ds3231_temp_reg_t tempval;
} ds3231_ram_regs_t;

/**
 * @brief Get integer value from received time keeping register value.
 */
static inline uint8_t ds3231_from_reg(ds3231_reg_t reg) {
  return (reg & 0xf) + (reg >> 4) * 10;
}

/**
 * @brief Put integer value to time keeping register value to send.
 */
static inline ds3231_reg_t ds3231_to_reg(uint8_t val) {
  return (val % 10) | ((val / 10) << 4);
}

/**
 * @brief Get hours value in 24h format from received time keeping register value.
 */
static inline uint8_t ds3231_from_reg_24h(ds3231_reg_t reg) {
  if (reg & DS3231_HOURS_12) {
    return ((reg & 0xf) + ((reg >> 4) & 0x1) * 10) << (reg & DS3231_HOURS_PM ? 1 : 0);
  } else {
    return (reg & 0xf) + ((reg >> 4) & 0x3) * 10;
  }
}

/**
 * @brief Put hours value in 24h format to time keeping register value to send.
 */
static inline ds3231_reg_t ds3231_to_reg_24h(uint8_t val) {
  return (val % 10) | (((val / 10) & 0x3) << 4);
}

/**
 * @brief Convert temperature from received temperature register value.
 */
static inline float ds3231_convert_temp(const ds3231_temp_reg_t *temp_reg) {
  int val = temp_reg->msb & 0x7f;
  if (temp_reg->msb & 0x80) val = -val;
  return (float)(temp_reg->lsb >> 6) / 4 * val;
}
