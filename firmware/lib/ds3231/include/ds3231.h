#ifndef __REALTIME_H__
#define __REALTIME_H__ "realtime.h"

#include <stdint.h>

void ds3231_init(void);
#define ds3231_done()

typedef struct {
  /* date */
  uint16_t year;
  uint8_t month;
  uint8_t date;
  uint8_t day;
  /* time */
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
} ds3231_time_t;

void ds3231_read(ds3231_time_t *time);

#define ds3231_field(name)                    \
  ((uint8_t)(intptr_t)&((const ds3231_time_t*)0)->name)

void ds3231_write_field(uint8_t field, uint16_t value);

#define ds3231_write(field, value) \
  ds3231_write_field(ds3231_field(field), value)

#endif /* __REALTIME_H__ */
