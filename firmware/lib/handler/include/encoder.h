typedef uint8_t encin_t;
typedef uint8_t encev_t;
typedef uint8_t enclen_t;

#define enc_flag(inputs, state) (((state) ? 0x80 : 0x00) | ((inputs) & 0x7f))
#define enc_flag_state(flag) (((flag) & 0x80) >> 7)
#define enc_flag_inputs(flag) ((flag) & 0x7f)

#define enc_ev(enc, dir) ((((enc) << 1) & 0xfe) | ((dir) & 0x01))
#define enc_ev_enc(ev) (((ev) >> 1) & 0x3f)
#define enc_ev_dir(ev) ((ev) & 0x01)
static const encev_t enc_none = 0xff;

typedef struct {
  encin_t a;
  encin_t b;
} encch_t;

typedef struct {
  uint8_t phases;
  encev_t ev;
} encst_t;

typedef struct {
  const encch_t *ch;
  encst_t *st;
  uint8_t flag;
} enccfg_t;

#define enc_in_def(name) static const encch_t name##_inputs[] =
#define enc_drv_def(name, state)                                        \
  static encst_t name##_states[sizeof(name##_inputs)/sizeof(name##_inputs[0])]; \
  static const enccfg_t name = {                                        \
    name##_inputs, name##_states,                                       \
    enc_flag(sizeof(name##_inputs)/sizeof(name##_inputs[0]), state) }

void enc_init(const enccfg_t *cfg);
void enc_done(const enccfg_t *cfg);
void enc_update(const enccfg_t *cfg);
encev_t enc_event(const enccfg_t *cfg);
