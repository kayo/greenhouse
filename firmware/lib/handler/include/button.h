typedef uint8_t btnin_t;
typedef uint8_t btnev_t;
typedef uint8_t btnlen_t;
typedef uint32_t btntime_t;

#define btn_flag(events, inputs, state) (((state) ? 0x80 : 0x00) | (((events) << 4) & 0x70) | ((inputs) & 0x0f))
#define btn_flag_state(flag) (((flag) & 0x80) >> 7)
#define btn_flag_events(flag) (((flag) & 0x70) >> 4)
#define btn_flag_inputs(flag) ((flag) & 0x0f)

#define btn_ev(btn, idx) ((((btn) << 4) & 0xf0) | ((idx) & 0x0f))
#define btn_ev_btn(ev) (((ev) >> 4) & 0x0f)
#define btn_ev_idx(ev) ((ev) & 0x0f)
static const btnev_t btn_none = 0xff;

typedef struct {
  btntime_t time;
  btnev_t ev;
} btnst_t;

typedef struct {
  const btntime_t *ev;
  const btnin_t *in;
  btnst_t *st;
  uint8_t flag;
} btncfg_t;

#define btn_ev_def(name) static const btntime_t name##_events[] =
#define btn_in_def(name) static const btnin_t name##_inputs[] =
#define btn_drv_def(name, state)                                        \
  static btnst_t name##_states[sizeof(name##_inputs)/sizeof(name##_inputs[0])]; \
  static const btncfg_t name = {                                        \
    name##_events, name##_inputs, name##_states,                        \
    btn_flag(sizeof(name##_events)/sizeof(name##_events[0]),            \
             sizeof(name##_inputs)/sizeof(name##_inputs[0]), state) }

void btn_init(const btncfg_t *cfg);
void btn_done(const btncfg_t *cfg);
void btn_update(const btncfg_t *cfg, btntime_t time);
btnev_t btn_event(const btncfg_t *cfg);
