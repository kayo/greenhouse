#include <stdint.h>
#include "button.h"
#include "ioport.h"

#define port_get io_port_get
#define port_on(port, pads, updown)                                     \
  io_port_in(port, pads, PULL_UPDOWN);                                  \
  updown ? io_port_set(port, pads) : io_port_clear(port, pads)
#define port_off(port, pads) io_port_in(port, pads, FLOAT)


static void _btn_mask_pads(const btncfg_t *cfg, iopad_t *pads){
  io_node_zero(pads);
  io_node_fill(pads, btn_flag_inputs(cfg->flag), cfg->in);
}

void btn_init(const btncfg_t *cfg){
  iopad_t pads[io_port_count()];
  iosize_t i;
  
  _btn_mask_pads(cfg, pads);

  for(i = 0; i < io_port_count(); i++){
    if(pads[i]) port_on(i, pads[i], !btn_flag_state(cfg->flag));
  }
}

void btn_done(const btncfg_t *cfg){
  iopad_t pads[io_port_count()];
  iosize_t i;
  
  _btn_mask_pads(cfg, pads);
  
  for(i = 0; i < io_port_count(); i++){
    if(pads[i]) port_off(i, pads[i]);
  }
}

void btn_update(const btncfg_t *cfg, btntime_t time){
  iopad_t pads[io_port_count()];
  iosize_t i;
  
  _btn_mask_pads(cfg, pads);
  
  for(i = 0; i < io_port_count(); i++){
    if(pads[i]) pads[i] = port_get(i) ^ (btn_flag_state(cfg->flag) ? 0x0 : ~0x0);
  }
  
  if(time == 0) time = 1;
  
  for(i = 0; i < btn_flag_inputs(cfg->flag); i++){
    btnst_t *st = &cfg->st[i];
    if((st->time != 0) != io_node_get(pads, cfg->in[i])){ /* button state changed */
      if(st->time == 0){ /* button pressed */
        st->time = time;
      }else{ /* button released */
        st->time = time - st->time;
        for(st->ev = 0; st->ev < btn_flag_events(cfg->flag) && st->time > cfg->ev[st->ev]; st->ev++);
        st->time = 0;
      }
    }
  }
}

btnev_t btn_event(const btncfg_t *cfg){
  iosize_t i;
  for(i = 0; i < btn_flag_inputs(cfg->flag); i++){
    btnst_t *st = &cfg->st[i];
    if(st->ev != 0){
      btnev_t ev = btn_ev(i, st->ev - 1);
      st->ev = 0;
      return ev;
    }
  }
  return btn_none;
}
