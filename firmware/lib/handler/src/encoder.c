#include <stdint.h>
#include "encoder.h"
#include "ioport.h"

#define port_get io_port_get
#define port_on(port, pads, updown)                                     \
  io_port_in(port, pads, PULL_UPDOWN);                                  \
  updown ? io_port_set(port, pads) : io_port_clear(port, pads)
#define port_off(port, pads) io_port_in(port, pads, FLOAT)

static void _enc_mask_pads(const enccfg_t *cfg, iopad_t *pads){
  io_node_zero(pads);
  io_node_fill(pads, enc_flag_inputs(cfg->flag)<<1, &cfg->ch[0].a);
}

void enc_init(const enccfg_t *cfg){
  iopad_t pads[io_port_count()];
  iosize_t i;
  
  _enc_mask_pads(cfg, pads);
  
  for(i = 0; i < io_port_count(); i++){
    if(pads[i]) port_on(i, pads[i], !enc_flag_state(cfg->flag));
  }
}

void enc_done(const enccfg_t *cfg){
  iopad_t pads[io_port_count()];
  iosize_t i;
  
  _enc_mask_pads(cfg, pads);
  
  for(i = 0; i < io_port_count(); i++){
    if(pads[i]) port_off(i, pads[i]);
  }
}

void enc_update(const enccfg_t *cfg){
  iopad_t pads[io_port_count()];
  iosize_t i;
  
  _enc_mask_pads(cfg, pads);
  
  for(i = 0; i < io_port_count(); i++){
    if(pads[i]) pads[i] = port_get(i) ^ (enc_flag_state(cfg->flag) ? 0x0 : ~0x0);
  }
  
  for(i = 0; i < enc_flag_inputs(cfg->flag); i++){
    encst_t *st = &cfg->st[i];
    
    /* get current phase */
    uint8_t phase = ((io_node_get(pads, cfg->ch[i].a) ? 1 : 0) << 1) | (io_node_get(pads, cfg->ch[i].b) ? 1 : 0);
    
    /* get previous phase */
    uint8_t _phase = st->phases & 0b11;
    
    if(phase != _phase){
      st->phases <<= 2;
      st->phases |= phase;
      
#define PHASES(_1, _2, _3, _4) (((_1)<<6)|((_2)<<4)|((_3)<<2)|((_4)<<0))
      
      switch(st->phases){
      case PHASES(0, 2, 3, 1):
      case PHASES(3, 2, 3, 1):
        st->ev = 2;
        break;
      case PHASES(3, 2, 0, 1):
      case PHASES(0, 2, 0, 1):
        st->ev = 1;
        break;
      }
    }
  }
}

encev_t enc_event(const enccfg_t *cfg){
  iosize_t i;
  for(i = 0; i < enc_flag_inputs(cfg->flag); i++){
    encst_t *st = &cfg->st[i];
    if(st->ev != 0){
      encev_t ev = enc_ev(i, st->ev - 1);
      st->ev = 0;
      return ev;
    }
  }
  return enc_none;
}
