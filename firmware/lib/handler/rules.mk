libhandler.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libhandler
libhandler.INHERIT := firmware libopencm3
libhandler.CDIRS := $(libhandler.BASEPATH)include
libhandler.SRCS := $(addprefix $(libhandler.BASEPATH)src/,\
  button.c \
  encoder.c)
