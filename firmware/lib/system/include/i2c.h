#ifndef __I2C_H__
#define __I2C_H__ "i2c.h"

#define i2c_fn(N, F) _CAT4(i2c, N, _, F)

#define i2c_def(N)                        \
  void i2c_fn(N, init)(void);             \
  void i2c_fn(N, done)(void);             \
  void i2c_fn(N, io)(uint8_t addr,        \
                     const uint8_t *wptr, \
                     uint8_t wlen,        \
                     uint8_t *rptr,       \
                     uint8_t rlen)

#endif /* __I2C_H__ */
