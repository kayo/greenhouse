#ifndef __IOPORT_H__
#define __IOPORT_H__ "ioport.h"

typedef uint32_t ioport_t;
typedef uint16_t iopad_t;
typedef uint8_t ionode_t;
typedef uint8_t iosize_t;

extern const ioport_t io_port_map[];
extern const iosize_t io_port_len;

#define io_port_count() io_port_len

#define io_port_def(ports...)               \
  const ioport_t io_port_map[] = { ports }; \
  const iosize_t io_port_len = sizeof(io_port_map) / sizeof(io_port_map[0]);

#define io_node(port, pad) ((((port) << 5) & 0xe0) | ((pad) & 0x1f))
#define io_node_port(out) (((out) >> 5) & 0x7)
#define io_node_pad(out) ((out) & 0x1f)

#define io_node_get(pads, node) (0 != (pads[io_node_port(node)] & (1 << io_node_pad(node))))
#define io_node_mask(pads, node) (pads[io_node_port(node)] |= 1 << io_node_pad(node))
#define io_node_unmask(pads, node) (pads[io_node_port(node)] &= ~(1 << io_node_pad(node)))

void io_node_zero(iopad_t *pads);
void io_node_fill(iopad_t *pads, iosize_t size, const ionode_t *node);

#ifdef USE_OPENCM3
#include <libopencm3/stm32/gpio.h>

#define io_port_get(port) gpio_port_read(io_port_map[port])
#define io_port_set(port, pads) gpio_set(io_port_map[port], pads)
#define io_port_clear(port, pads) gpio_clear(io_port_map[port], pads)
#define io_port_in(port, pads, mode) gpio_set_mode(io_port_map[port], GPIO_MODE_INPUT, GPIO_CNF_INPUT_##mode, pads)
#define io_port_out(port, pads, clock, mode) gpio_set_mode(io_port_map[port], GPIO_MODE_OUTPUT##clock, GPIO_CNF_OUTPUT_##mode, pads)
#endif

#endif /* __IOPORT_H__ */
