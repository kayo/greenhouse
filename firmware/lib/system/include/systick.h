#ifndef SYSTICK_H
#define SYSTICK_H "systick.h"

void systick_init(void);
void systick_done(void);

#endif /* SYSTICK_H */
