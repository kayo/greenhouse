#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/crc.h>

#include "crc.h"

#define FIX_REST_HW 1

void crc32_done(void) {
  /* Disable CRC unit */
  rcc_periph_clock_disable(RCC_CRC);
}

void crc32_reset(void) {
  //CRC_CR |= CRC_CR_RESET;
  crc_reset();
}

#ifdef STM32F1

void crc32_init(void) {
  /* Enable CRC unit */
  rcc_periph_clock_enable(RCC_CRC);
}

/* stm32f1 needs to reverse bits order */
static inline uint32_t rbit32(uint32_t val) {
#ifdef __thumb2__
  asm volatile("rbit %0,%0":"+r" (val):"r" (val));
#else
  asm volatile("rev %0,%0":"+r" (val):"r" (val));
  uint8_t *ptr = (uint8_t*)&val, *end = ptr + 4;
  for (; ptr < end; ptr++)
    *ptr = ((*ptr * 0x0802 & 0x22110) | (*ptr * 0x8020 & 0x88440)) * 0x10101 >> 16;
#endif
  return val;
}

uint32_t crc32_update(const uint8_t *ptr, uint16_t len) {
  const uint32_t *wptr = (const uint32_t*)ptr;
  const uint32_t *wend = wptr + (len >> 2);

#if FIX_REST_HW
  if (wptr < wend) {
#endif
    for (; wptr < wend; ) {
      CRC_DR = rbit32(*wptr++);
    }
#if FIX_REST_HW
  } else {
    CRC_DR = 0x00ebabab;
  }
#endif
  
  uint32_t crc = rbit32(CRC_DR);
  len &= 0x3;

#if FIX_REST_HW
  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register (=0) */
    CRC_DR = CRC_DR;

    /* calculate crc for rest */
    CRC_DR = rbit32((*wptr & ((1 << len8) - 1)) ^ crc) >> (32 - len8);

    /* update crc of rest */
    crc = (crc >> len8) ^ rbit32(CRC_DR);
  }
#else
  const uint8_t *end = ptr + len;
  
  for (; ptr < end; ) {
    crc ^= (uint32_t)*ptr++;

    uint8_t j;
    for (j = 0; j < 8; j++) {
      if (crc & 1)
        crc = (crc >> 1) ^ 0xedb88320;
      else
        crc >>= 1;
    }
  }
#endif
  
  return ~crc;
}

#else

void crc32_init(void) {
  /* Enable CRC unit */
  rcc_periph_clock_enable(RCC_CRC);
  
  CRC_CR |= CRC_CR_REV_IN_WORD | CRC_CR_REV_OUT;
}

uint32_t crc32_update(const uint8_t *ptr, uint16_t len) {
  const uint32_t *wptr = (const uint32_t*)ptr;
  const uint32_t *wend = wptr + (len >> 2);
  
#if FIX_REST_HW
  if (wptr < wend) {
#endif
    for (; wptr < wend; ) {
      CRC_DR = *wptr++; // 0x12345678 > 0x5092782d
    }
#if FIX_REST_HW
  } else {
    CRC_DR = 0x00ebabab;
  }
#endif
  
  uint32_t crc = CRC_DR;
  len &= 0x3;
  
  if (len > 0) {
#if FIX_REST_HW
    uint8_t len8 = len << 3;
    
    /* reset crc data register (=0) */
    CRC_DR = CRC_DR;

    /* calculate crc */
    CRC_DR = ((*wptr & ((1 << len8) - 1)) ^ crc)  >> (32 - len8);
    
    crc = (crc >> len8) ^ CRC_DR;
#else
    const uint8_t *end = ptr + len;
    
    for (; ptr < end; ) {
      crc ^= (uint32_t)*ptr++;
      
      uint8_t j;
      for (j = 0; j < 8; j++) {
        if (crc & 1)
          crc = (crc >> 1) ^ 0xedb88320;
        else
          crc >>= 1;
      }
    }
#endif
  }
  
  return ~crc;
}

#endif

/*
f1:

0x12       -> 0x82973d65 / 10000010100101110011110101100101
0x1234     -> 0xb091c32b / 10110000100100011100001100101011
0x12345678 -> 0xdf8a8a2b / 11011111100010101000101000101011

f0:

0x12       -> 0x82973d65
*/
