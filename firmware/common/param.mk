# libparam (parameter-based control)

libparam.BASEPATH ?= $(BASEPATH)libparam
libparam.store ?= opencm3-stm32-flash

PARAM_HAS ?= enum sint uint real fix min max stp def virt name unit desc hint pers
PARAM_MOD += param param-sio param-json jsmn/jsmn $(if $(libparam.store),param-store)

PARAM_DEF := enum:ENUM sint:SINT uint:UINT real:REAL cstr:CSTR hbin:HBIN ipv4:IPV4 mac:MAC dbl:DBL fix:FIX min:MIN max:MAX stp:STP def:DEF virt:VIRT name:NAME unit:UNIT desc:DESC hint:HINT pers:PERS

TARGET.LIBS += libparam
libparam.INHERIT = firmware libopencm3
libparam.SRCS += $(patsubst %,$(libparam.BASEPATH)/%.c,$(PARAM_MOD))
libparam.CDEFS += $(foreach x,$(filter $(addsuffix :%,$(PARAM_HAS)),$(PARAM_DEF)),PARAM_$(lastword $(subst :, ,$(x)))=1)
libparam.CDEFS += $(if $(libparam.store),PARAM_STORE=$(libparam.store))
libparam.CDIRS += $(libparam.BASEPATH)
