debug.COPT ?= g
debug.CDBG ?= gdb3
debug.CDEFS += USE_DEBUG

release.COPT ?= s
release.CDBG ?= gdb3
release.CDEFS += MONITOR_USE_SLEEP

common.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))
common.INHERIT ?= $(if $(filter y,$(DEBUG)),debug,release)
common.CSTD ?= gnu99
common.CDIRS ?= $(common.BASEPATH)include
common.CDEFS ?= _GNU_SOURCE
common.CWARN ?= all extra shadow undef implicit-function-declaration redundant-decls missing-prototypes strict-prototypes no-pointer-sign pointer-arith
common.COPTS ?= function-sections data-sections no-exceptions no-common
