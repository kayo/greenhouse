#include <avr/io.h>

int main() {
  DDRB = 0
    | _BV(PB0) /* Set PB0 as output */
    | _BV(PB1) /* Set PB1 as output */
    ;
  PORTB = 0
    | _BV(PB1) /* Set PB1 to logic one */
    ;
  TCCR0A = 0
    | _BV(COM0A0) /* Toggle OC0A on compare match */
    | _BV(WGM01) /* Select CTC wave generation mode */
    ;
  TCCR0B = 0
    | _BV(CS00) /* No prescaling is used */
    ;
  TCNT0 = 0;
  OCR0A = 0;
  
  for (;;);
}
