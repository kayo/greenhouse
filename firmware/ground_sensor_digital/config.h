#define chip_select B,1,0

#define timer_channel 0,B

/* soft|hard, baudrate */
#define uart_config soft,115200
#define uart_tx_pad B,3

/* analog channels config: port,pin,adc_channel,prepare_delay_us */
#define light_input B,5,0,5000
#define humidity_input B,2,1,5000
#define temperature_input B,4,2,5000
#define meander_output B,0,0,A
/* buffer length, median filter count */
#define sensor_config 16,8
