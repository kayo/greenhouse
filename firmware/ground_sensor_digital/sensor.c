#include "macro.h"
#include "config.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "sensor.h"

#define REFERENCE_SELECT _BV(REFS0)

#ifdef light_input
#  define LIGHT_CHANNEL _NTH2(light_input)
#  define light_prepare() _delay_ms(_NTH3(light_input)/1e3)
#endif

#ifdef humidity_input
#  define HUMIDITY_CHANNEL _NTH2(humidity_input)
#  define humidity_prepare() _delay_ms(_NTH3(humidity_input)/1e3)
#endif

#ifdef temperature_input
#  define TEMPERATURE_CHANNEL _NTH2(temperature_input)
#  define temperature_prepare() _delay_ms(_NTH3(temperature_input)/1e3)
#endif

#ifdef meander_output
#  define MEANDER_PIN _BV(_NTH1(meander_output))
#  define MEANDER_PORT _CAT2(PORT, _NTH0(meander_output))
#  define MEANDER_DDR _CAT2(DDR, _NTH0(meander_output))
#  define MEANDER_TCCRA _CAT3(TCCR, _NTH2(meander_output), A)
#  define MEANDER_TCCRB _CAT3(TCCR, _NTH2(meander_output), B)
#  define MEANDER_TCNT _CAT2(TCNT, _NTH2(meander_output))
#  define MEANDER_OCR _CAT3(TCCR, _NTH2(meander_output), _NTH3(meander_output))
#  define MEANDER_TCCRA_COM _BV(_CAT3(COM, _NTH2(meander_output), A0))

#  define meander_out() (MEANDER_DDR |= MEANDER_PIN)
#  define meander_off() (MEANDER_DDR &= ~MEANDER_PIN)
#  define meander_set() (MEANDER_PORT |= MEANDER_PIN)
#  define meander_clr() (MEANDER_PORT &= ~MEANDER_PIN)
#else
#  define meander_out()
#  define meander_off()
#  define meander_set()
#  define meander_clr()
#endif

#define sensor_enable() (ADCSRA |= _BV(ADEN))
#define sensor_disable() (ADCSRA &= ~_BV(ADEN))

#define BUFFER_LENGTH _NTH0(sensor_config)

#if _NTH1(sensor_config) > 0
#  define MEDIAN_FILTER _NTH1(sensor_config)
#  if _NTH1(sensor_config) > 1
#    define AVERAGE_FILTER 1
#  endif
#else
#  if _NTH0(sensor_config) > 1
#    define AVERAGE_FILTER 1
#  endif
#endif

ISR(ADC_vect) {}

void sensor_init(void) {
#define ADC_0_D ADC0D
#define ADC_1_D ADC1D
#define ADC_2_D ADC2D
#define ADC_3_D ADC3D
  
  DIDR0 = 0 /* Disable digital inputs on analog pins to save energy */
#ifdef LIGHT_CHANNEL
    | _BV(_CAT3(ADC_, LIGHT_CHANNEL, _D))
#endif
#ifdef TEMPERATURE_CHANNEL
    | _BV(_CAT3(ADC_, TEMPERATURE_CHANNEL, _D))
#endif
#ifdef HUMIDITY_CHANNEL
    | _BV(_CAT3(ADC_, HUMIDITY_CHANNEL, _D))
#endif
    ;

  ADCSRA = 0
    | _BV(ADIE) /* Enable conversion interrupt */
    | _BV(ADPS1) | _BV(ADPS0) /* prescaler=8 (see datasheet, p.84) */
    ;
  ADCSRB = 0;
}

static void quick_sort(sample_t a[], uint8_t n) {
  uint8_t i, j;
  sample_t p, t;
  
  if (n < 2)
    return;
  
  p = a[n / 2];
  
  for (i = 0, j = n - 1;; i++, j--) {
    while (a[i] < p)
      i++;
    while (p < a[j])
      j--;
    if (i >= j)
      break;
    t = a[i];
    a[i] = a[j];
    a[j] = t;
  }
  
  quick_sort(a, i);
  quick_sort(a + i, n - i);
}

static sample_t sensor_measure(void) {
  sample_t buffer[BUFFER_LENGTH];
  sample_t res = 0, *ptr = &buffer[0], *end = &buffer[BUFFER_LENGTH];
  
  sensor_enable(); /* Enable analog to digital converter */

  for (; ptr < end; ) {
    sleep_cpu();
    *ptr++ = ADC << 2;
  }

  sensor_disable(); /* Disable analog to digital converter */
  
#if defined(MEDIAN_FILTER) && MEDIAN_FILTER > 0
# define cnt MEDIAN_FILTER
  ptr = buffer + cnt / 2;
# define end (buffer + cnt / 2 + cnt)
  quick_sort(buffer, BUFFER_LENGTH);
#else /* !MEDIAN_FILTER */
# define cnt BUFFER_LENGTH
  ptr = buffer;
# define end (buffer + BUFFER_LENGTH)
#endif

#if defined(AVERAGE_FILTER) && AVERAGE_FILTER > 0
  for (; ptr < end; res += *ptr++);
  res /= cnt;
#else
  res = *ptr;
#endif

  return res;
}

sample_t sensor_read(channel_t channel) {
  sample_t value = 0;
  
  meander_set(); /* Set humidity measure control pin */
  meander_out(); /* Configure humidity measure control pin as output */
  
  switch (channel) {
#ifdef LIGHT_CHANNEL
  case sensor_light:
    light_prepare();
    
    ADMUX
      = REFERENCE_SELECT
      | LIGHT_CHANNEL /* select channel */
      ;

    value = sensor_measure();
    
    break;
#endif
#ifdef TEMPERATURE_CHANNEL
  case sensor_temperature:
    temperature_prepare();
    
    ADMUX
      = REFERENCE_SELECT
      | TEMPERATURE_CHANNEL /* select channel */
      ;

    value = sensor_measure();
    
    break;
#endif
#ifdef HUMIDITY_CHANNEL
  case sensor_humidity:
    MEANDER_TCNT = 0; /* Reset counter */
    MEANDER_OCR = 0; /* Set max output frequency (F_CPU/2) */
    
    MEANDER_TCCRA = 0
      | MEANDER_TCCRA_COM /* Toggle OC0A on compare match */
      | _BV(WGM01) /* Select CTC wave generation mode */
      ;
    MEANDER_TCCRB = 0
      | _BV(CS00) /* Enable counter without prescaler */
      ;

    humidity_prepare();
    
    ADMUX
      = REFERENCE_SELECT
      | HUMIDITY_CHANNEL /* select channel */
      ;

    value = sensor_measure();

    MEANDER_TCCRB = 0; /* Disable counter */
    MEANDER_TCCRA = 0; /* Disable output */
    
    break;
#endif
  }
  
  meander_clr(); /* Clear humidity measure control pin */
  meander_off(); /* Configure humidity measure control pin as input */

  return value;
}
