#ifndef _UART_H_

#include <stdint.h>

void uart_init(void);
void uart_send(uint8_t *ptr, uint8_t len);

#endif/*_UART_H_*/
