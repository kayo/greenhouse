#include "macro.h"
#include "config.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#include "uart.h"
#include "sensor.h"

#define CS_PIN _BV(_NTH1(chip_select))
#define CS_PORT _CAT2(PORT, _NTH0(chip_select))
#define CS_DDR _CAT2(DDR, _NTH0(chip_select))
#define CS_INT _BV(_CAT2(INT, _NTH2(chip_select)))

ISR(INT0_vect) {}

static inline void cs_init(void) {
  CS_PORT |= CS_PIN; /* Enable pull-up resistor */
  /* The low level generates an interrupt request */
  //MCUCR |= _BV(ISC01); /* The falling edge generates an interrupt request */
}

#define cs_enable() GIMSK |= CS_INT; /* Enable external interrupt */
#define cs_disable() GIMSK &= ~CS_INT; /* Disable external interrupt */

//#define TEST_UART

int main() {
  uart_init();

#ifdef TEST_UART
  for (;;) {
    uint8_t data[] = { 0b01010101 };
    uart_send(data, sizeof(data));
  }
#endif
  
  cs_init();
  sensor_init();
  
  sei();
  sleep_enable();

  for (;;) {
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    
    cs_enable();
    sleep_cpu();
    cs_disable();

    set_sleep_mode(SLEEP_MODE_ADC);
    
    {
      union {
        sample_t data;
        uint8_t byte[sizeof(sample_t)];
      } value;
      
      value.data = sensor_read(sensor_humidity);
      uart_send(value.byte, sizeof(value.byte)/sizeof(value.byte[0]));
      
      value.data = sensor_read(sensor_temperature);
      uart_send(value.byte, sizeof(value.byte)/sizeof(value.byte[0]));
      
      value.data = sensor_read(sensor_light);
      uart_send(value.byte, sizeof(value.byte)/sizeof(value.byte[0]));
    }
  }
}
