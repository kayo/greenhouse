#include "macro.h"
#include "config.h"

#include <avr/io.h>
#include "uart.h"

#define _UART_soft 1
#define _UART_hard 2

#define UART_TYPE _CAT2(_UART_, _NTH0(uart_config))

#if UART_TYPE == _UART_soft
#include <util/delay.h>
#endif

#if UART_TYPE == _UART_tim0
#define UART_USE_TIM0
#endif

#define UART_BAUD_RATE _NTH1(uart_config)

#define UART_TX_PIN _BV(_NTH1(uart_tx_pad))
#define UART_TX_PORT _CAT2(PORT, _NTH0(uart_tx_pad))
#define UART_TX_DDR _CAT2(DDR, _NTH0(uart_tx_pad))

#define uart_tx_out() (UART_TX_DDR |= UART_TX_PIN)
#define uart_tx_off() (UART_TX_DDR &= ~UART_TX_PIN)
#define uart_tx_set() (UART_TX_PORT |= UART_TX_PIN)
#define uart_tx_res() (UART_TX_PORT &= ~UART_TX_PIN)

void uart_init(void) {
#ifdef UART_USE_TIM0
  TCCR0A = _BV(WGM01);	// compare mode
  TCCR0B = _BV(CS00);		// prescaler 1
  OCR0A = (F_CPU / UART_BAUD_RATE) - 1; /* Set baudrate */
#endif

  uart_tx_set();
  uart_tx_out();
}

static inline void uart_delay_loop(void) {
#ifdef UART_USE_TIM0
  for (; !(TIFR0 & _BV(OCF0A)); );
  TIFR0 |= _BV(OCF0A);
#else
  _delay_us(1e6 / UART_BAUD_RATE - 11e6 / F_CPU);
#endif
}

/* bitbanged UART transmit byte */
static void uart_send_byte(uint8_t data) {
  uint8_t i;

#ifdef UART_USE_TIM0
  TCCR0B = 0;
  TCNT0 = 0;
  TIFR0 |= _BV(OCF0A);
  TCCR0B |= _BV(CS00);
  TIFR0 |= _BV(OCF0A);
#endif
  
  uart_tx_res();
  uart_delay_loop();
  
  for (i = 0; i < 8; i++) {
    if (data & 1) {
      uart_tx_set();
    } else {
      uart_tx_res();
    }
    data >>= 1;
    uart_delay_loop();
  }
  
  uart_tx_res();
  uart_delay_loop();
}

void uart_send(uint8_t *ptr, uint8_t len) {
  uint8_t *end = ptr + len;
  for (; ptr < end; uart_send_byte(*ptr++));
}
