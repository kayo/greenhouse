#ifndef _SENSOR_H_

#include <stdint.h>

enum {
  sensor_light,
  sensor_temperature,
  sensor_humidity,
};

typedef uint8_t channel_t;
typedef int16_t sample_t;

void sensor_init(void);
sample_t sensor_read(channel_t channel);

#endif /*_SENSOR_H_*/
