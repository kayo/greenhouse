/* common config */
#define mcu_frequency 48
#define systick_config 1,100 /* divider,timeout [mS] */

#define sensor_adc 1,12,1.2,3.3 /* adc device,adc bits,adc Vref,adc Vtop */
#define sensor_dma 1,1 /* device,channel */
#define photo_input A,0,0 /* port,pad,channel */
#define therm_input A,6,6 /* port,pad,channel */
#define humid_input A,7,7 /* port,pad,channel */

#define filter_config 24,16 /* average samples,median samples */

//#define therm_config 4,(4.7e3,0),SH,(0.0013081992342927878,0.00005479611105636211,9.415113523239857e-7) /* fraction bits,resistors(r1,r2),model,params(a,b,c) */
#define therm_config 4,(4.7e3,0),BETA,(3450,10e3,C2K(24)) /* fraction bits,resistors(r1,r2),model,params(beta,r0,t0) */

#define vref_config 10 /* fraction bits */
#define tmcu_config 4,30,1.43,4.3 /* fraction bits,T0,Vsense(T0),Avg Slope */

#define photo_output A,10
#define therm_output B,1 /* port,pad */
#define status_output A,5 /* port,pad */

#define modul_output A,4,4 /* port,pad,altfn */
#define modul_timer 14,1 /* device,channel */

#define console_uart 1,19200,8 /* device,baud,bits */
#define console_dma 1,2,3,256 /* device,tx_channel,rx_channel,buffer */
#define console_gpio (A,3,1),(A,2,1),(A,1,1) /* RX,TX,DE */
