#ifndef __SENSOR_H__
#define __SENSOR_H__ "sensor.h"

void sensor_init(void);
void sensor_done(void);
void sensor_step(void);

#endif /* __SENSOR_H__ */
