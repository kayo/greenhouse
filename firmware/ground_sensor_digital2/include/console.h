#ifndef __CONSOLE_H__
#define __CONSOLE_H__ "console.h"

typedef int console_cmd_f(const int argc, const char *argv[]);

typedef struct {
  const char *name;
  console_cmd_f *func;
} console_cmd_t;

void
console_init(void);

void
console_done(void);

void
console_step(void);

#endif /* __CONSOLE_H__ */
