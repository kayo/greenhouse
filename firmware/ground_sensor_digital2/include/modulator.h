#ifndef __MODULATOR_H__
#define __MODULATOR_H__ "modulator.h"

void
modulator_init(void);

void
modulator_done(void);

void
modulator_set(uint8_t div);

#endif /* __MODULATOR_H__ */
