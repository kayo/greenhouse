#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

#include "systick.h"
#include "monitor.h"
#include "crc.h"
#include "console.h"
#include "sensor.h"
#include "modulator.h"

#include "proto.h"
#include "macro.h"
#include "config.h"

#define PHOTO_EN_PORT _CAT2(GPIO, _NTH0(photo_output))
#define PHOTO_EN_PAD _CAT2(GPIO, _NTH1(photo_output))
#define THERM_EN_PORT _CAT2(GPIO, _NTH0(therm_output))
#define THERM_EN_PAD _CAT2(GPIO, _NTH1(therm_output))
#define STATUS_PORT _CAT2(GPIO, _NTH0(status_output))
#define STATUS_PAD _CAT2(GPIO, _NTH1(status_output))

typedef enum { OFF, ON, TOGGLE } output_op_t;

static void
output_set(uint32_t port, uint16_t pads, output_op_t op) {
  switch (op) {
  case ON: gpio_set(port, pads); break;
  case OFF: gpio_clear(port, pads); break;
  case TOGGLE: gpio_toggle(port, pads); break;
  }
}

static inline void
status_set(output_op_t op) {
  output_set(STATUS_PORT, STATUS_PAD, op);
}

static inline void
photo_en(output_op_t op) {
  output_set(PHOTO_EN_PORT, PHOTO_EN_PAD, op);
}

static inline void
therm_en(output_op_t op) {
  output_set(THERM_EN_PORT, THERM_EN_PAD, op);
}

static void
output_conf(uint32_t port, uint16_t pads) {
  gpio_mode_setup(port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, pads);
  gpio_set_output_options(port, GPIO_OTYPE_PP, GPIO_OSPEED_LOW, pads);
}

static inline void
output_init(void) {
  output_conf(PHOTO_EN_PORT, PHOTO_EN_PAD);
  output_conf(THERM_EN_PORT, THERM_EN_PAD);
  output_conf(STATUS_PORT, STATUS_PAD);
}

monitor_def();

static inline void
hardware_init(void) {
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);

  /* Enable DMA for ADC and UART */
  rcc_periph_clock_enable(RCC_DMA);

#if 0
  /* Config UART1 de/tx/rx */
  gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE,
                  GPIO1 | GPIO2 | GPIO3);
  gpio_set_af(GPIOA, GPIO_AF1,
              GPIO1 | GPIO2 | GPIO3);
  gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          GPIO1 | GPIO2);
  //gpio_mode_setup(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE,
  //                GPIO3);
#endif
}

static inline void
init(void) {
  rcc_clock_setup_in_hsi_out_48mhz();
  
  hardware_init();
  systick_init();
  sensor_init();
  output_init();
  modulator_init();
  crc32_init();
  console_init();
}

proto_val_t dev_val;

int
main(void) {
  init();

  monitor_init();
  photo_en(ON);
  therm_en(ON);
  modulator_set(1);

  for (;;) {
    status_set(TOGGLE);
    monitor_wait();

    console_step();
    sensor_step();
  }
}
