#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "modulator.h"
#include "macro.h"
#include "config.h"

#define OUTPUT_PORT _CAT2(GPIO, _NTH0(modul_output))
#define OUTPUT_PAD _CAT2(GPIO, _NTH1(modul_output))
#define OUTPUT_AF _CAT2(GPIO_AF, _NTH2(modul_output))

#define TIMER_RCC _CAT2(RCC_TIM, _NTH0(modul_timer))
#define TIMER_DEV _CAT2(TIM, _NTH0(modul_timer))
#define TIMER_OCN _CAT2(TIM_OC, _NTH1(modul_timer))

static inline void pwm_oc_enable(uint32_t timer, enum tim_oc_id oc_id) {
  /* Disable output */
	timer_disable_oc_output(timer, oc_id);

  /* Configure global mode of line */
	timer_disable_oc_clear(timer, oc_id);
	timer_enable_oc_preload(timer, oc_id);
	timer_set_oc_slow_mode(timer, oc_id);
	timer_set_oc_mode(timer, oc_id, TIM_OCM_PWM1);
  
	/* Configure OC */
	timer_set_oc_polarity_high(timer, oc_id);
	timer_set_oc_idle_state_unset(timer, oc_id);
  
	/* Set the capture compare value for OC */
	timer_set_oc_value(timer, oc_id, 0);

  /* Enable output */
	timer_enable_oc_output(timer, oc_id);
}

static inline void pwm_oc_disable(uint32_t timer, enum tim_oc_id oc_id) {
  /* Disable output */
	timer_disable_oc_output(timer, oc_id);
}

void modulator_init(void) {
  /* Config modulation output */
  gpio_mode_setup(OUTPUT_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE,
                  OUTPUT_PAD);
  gpio_set_output_options(OUTPUT_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          OUTPUT_PAD);
  gpio_set_af(OUTPUT_PORT, OUTPUT_AF,
              OUTPUT_PAD);
  
  /* Enable TIMER_DEV clock */
  rcc_periph_clock_enable(TIMER_RCC);
  
  /* Reset TIM peripheral */
  timer_reset(TIMER_DEV);

  /* Timer global mode:
	 * - No divider
	 * - Alignment edge
	 * - Direction up
	 */
  timer_set_mode(TIMER_DEV, TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

  /* Reset prescaler value */
  timer_set_prescaler(TIMER_DEV, 0);

  /* Enable preload */
  timer_enable_preload(TIMER_DEV);

  /* Continuous mode */
  timer_continuous_mode(TIMER_DEV);
  
  timer_set_period(TIMER_DEV, 1);
  
  /* Configure break and deadtime */
  /*
	timer_set_enabled_off_state_in_idle_mode(TIMER_DEV);
	timer_set_enabled_off_state_in_run_mode(TIMER_DEV);
	timer_disable_break(TIMER_DEV);
	timer_set_break_polarity_high(TIMER_DEV);
	timer_disable_break_automatic_output(TIMER_DEV);
	timer_set_break_lock(TIMER_DEV, TIM_BDTR_LOCK_OFF);
  */

  pwm_oc_enable(TIMER_DEV, TIMER_OCN);

  /* ARR reload enable. */
	timer_enable_preload(TIMER_DEV);

  /*
	 * Enable preload of complementary channel configurations and
	 * update on COM event.
	 */
	//timer_enable_preload_complementry_enable_bits(TIMER_DEV);

	/* Enable outputs in the break subsystem. */
	//timer_enable_break_main_output(TIMER_DEV);
  
	/* Counter enable. */
	timer_enable_counter(TIMER_DEV);
}

void modulator_done(void) {
  pwm_oc_disable(TIMER_DEV, TIMER_OCN);
  
  /* Counter disable */
	timer_disable_counter(TIMER_DEV);
  
  /* Disable TIMER_DEV clock */
  rcc_periph_clock_disable(TIMER_RCC);
}

void
modulator_set(uint8_t val) {
  timer_set_oc_value(TIMER_DEV, TIMER_OCN, val);
}
