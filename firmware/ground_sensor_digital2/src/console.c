#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include "console.h"
#include "proto.h"
#include "macro.h"
#include "config.h"

#define UART_RCC _CAT2(RCC_USART, _NTH0(console_uart))
#define UART_DEV _CAT2(USART, _NTH0(console_uart))
#define UART_BAUD _NTH1(console_uart)
#define UART_BITS _NTH2(console_uart)

#define DMA_DEV _CAT2(DMA, _NTH0(console_dma))
#define DMA_TX_CH _CAT2(DMA_CHANNEL, _NTH1(console_dma))
#define DMA_RX_CH _CAT2(DMA_CHANNEL, _NTH2(console_dma))
#define DMA_TX_IRQ _CAT5(NVIC_DMA, _NTH0(console_dma), _CHANNEL, _NTH1(console_dma), _IRQ)
#define dma_tx_isr _CAT5(dma, _NTH0(console_dma), _channel, _NTH1(console_dma), _isr)
#define DMA_BUF _NTH3(console_dma)

#ifdef NVIC_DMA1_CHANNEL2_3_IRQ
#define dma1_channel2_isr dma1_channel2_3_isr
#define dma1_channel3_isr dma1_channel2_3_isr
#define NVIC_DMA1_CHANNEL2_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#define NVIC_DMA1_CHANNEL3_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#endif

#ifdef NVIC_DMA1_CHANNEL4_5_IRQ
#define dma1_channel4_isr dma1_channel4_5_isr
#define dma1_channel5_isr dma1_channel4_5_isr
#define NVIC_DMA1_CHANNEL4_IRQ NVIC_DMA1_CHANNEL4_5_IRQ
#define NVIC_DMA1_CHANNEL5_IRQ NVIC_DMA1_CHANNEL4_5_IRQ
#endif

#ifdef STM32F1

#else /* STM32F1 */

#define RX_GPIO _UNWR(_NTH0(console_gpio))
#define RX_PORT _CAT2(GPIO, _NTH0(RX_GPIO))
#define RX_PAD _CAT2(GPIO, _NTH1(RX_GPIO))

#define TX_GPIO _UNWR(_NTH1(console_gpio))
#define TX_PORT _CAT2(GPIO, _NTH0(TX_GPIO))
#define TX_PAD _CAT2(GPIO, _NTH1(TX_GPIO))

#define DE_GPIO _UNWR(_NTH2(console_gpio))
#define DE_PORT _CAT2(GPIO, _NTH0(DE_GPIO))
#define DE_PAD _CAT2(GPIO, _NTH1(DE_GPIO))

#endif /* STM32F1 */

#ifdef STM32F1

#define USART_TDR USART_DR
#define USART_RDR USART_DR

#else /* STM32F1 */

#define USART_STOPBITS_1 USART_CR2_STOP_1_0BIT
#define USART_SR_IDLE USART_ISR_IDLE
#define USART_SR_NE USART_ISR_NF
#define USART_SR_FE USART_ISR_FE
#define USART_SR_ORE USART_ISR_ORE

#endif /* STM32F1 */

static uint8_t buffer[DMA_BUF]; /* i/o data buffer */

static void tx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
}

static void tx_enable(uint16_t size) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_TX_CH, size);
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_TX_CH);
}

static void rx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
}

static void rx_enable(void) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_RX_CH, sizeof(buffer));
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_RX_CH);
}

#ifdef STM32F1
#define usart_clear_flag(usart, flag)
#else
static void usart_clear_flag(uint32_t usart, uint32_t flag) {
  USART_ICR(usart) |= flag;
}
#endif

void console_init(void) {
  rcc_periph_clock_enable(UART_RCC);
  
  nvic_enable_irq(DMA_TX_IRQ);

#if 1
  /* I/O configuration */
#ifdef STM32F1

#else
  gpio_mode_setup(RX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, RX_PAD);
  gpio_set_af(RX_PORT, GPIO_AF1, RX_PAD);
  gpio_set_output_options(RX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, RX_PAD);
  
#if TX_PORT == DE_PORT
  gpio_mode_setup(TX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, TX_PAD | DE_PAD);
  gpio_set_af(TX_PORT, GPIO_AF1, TX_PAD | DE_PAD);
  gpio_set_output_options(TX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, TX_PAD | DE_PAD);
#else
  gpio_mode_setup(TX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, TX_PAD);
  gpio_set_af(TX_PORT, GPIO_AF1, TX_PAD);
  gpio_set_output_options(TX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, TX_PAD);
  
  gpio_mode_setup(DE_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, DE_PAD);
  gpio_set_af(DE_PORT, GPIO_AF1, DE_PAD);
  gpio_set_output_options(DE_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, DE_PAD);
#endif
  
#endif
#endif

  /* configure uart */
  usart_set_baudrate(UART_DEV, UART_BAUD);
  usart_set_databits(UART_DEV, UART_BITS);
  usart_set_parity(UART_DEV, USART_PARITY_NONE);
  usart_set_stopbits(UART_DEV, USART_STOPBITS_1);
  usart_set_mode(UART_DEV, USART_MODE_TX_RX);
  usart_set_flow_control(UART_DEV, USART_FLOWCONTROL_NONE);

  /* rs485 */
#ifndef STM32F1
  USART_CR3(UART_DEV) &= ~(USART_CR3_DEP); // | USART_CR3_OVRDIS | USART_CR3_ONEBIT);
  USART_CR3(UART_DEV) |= USART_CR3_DEM | USART_CR3_DDRE;
  //USART_CR2(UART_DEV) |= USART_CR2_TXINV | USART_CR2_RXINV;
#endif

  /* TX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_TX_CH);
  dma_set_priority(DMA_DEV, DMA_TX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_TX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_TX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_TX_CH);
  dma_set_read_from_memory(DMA_DEV, DMA_TX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_TX_CH, (uint32_t)&USART_TDR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_TX_CH, (uint32_t)buffer);
  
  /* RX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_RX_CH);
  dma_set_priority(DMA_DEV, DMA_RX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_RX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_RX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_RX_CH);
  dma_enable_circular_mode(DMA_DEV, DMA_RX_CH);
  dma_set_read_from_peripheral(DMA_DEV, DMA_RX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_RX_CH, (uint32_t)&USART_RDR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_RX_CH, (uint32_t)buffer);
  
  /* Enable TX transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);

  /* Enable TX transfer error interrupt */
  /*dma_enable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  
  usart_enable_tx_dma(UART_DEV);
  usart_enable_rx_dma(UART_DEV);
  
  usart_enable(UART_DEV);
  rx_enable();
  
  //usart_clear_flag(UART_DEV,
  //                 USART_ICR_IDLECF);
}

void console_done(void) {
  usart_disable(UART_DEV);
  
  usart_disable_tx_dma(UART_DEV);
  usart_disable_rx_dma(UART_DEV);
  
  dma_disable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);

  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
  
  nvic_disable_irq(DMA_TX_IRQ);

  rcc_periph_clock_disable(UART_RCC);
}

void dma_tx_isr(void) {
  /*bool error = dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TEIF);*/
  dma_clear_interrupt_flags(DMA_DEV, DMA_TX_CH, DMA_GIF | DMA_TEIF | DMA_TCIF);
  tx_disable();
  rx_enable();
}

static uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

static uint32_t dma_count_data(uint32_t dma, uint8_t channel) {
  return DMA_CNDTR(dma, channel);
}

void console_step(void) {
  if (!dma_is_active(DMA_DEV, DMA_RX_CH) ||
      !usart_get_flag(UART_DEV,
                      USART_SR_IDLE)) {
    return;
  }
  
  usart_clear_flag(UART_DEV,
                   USART_ICR_IDLECF);
  
  uint16_t length;

  if (usart_get_flag(UART_DEV, 
                     USART_SR_NE |
                     USART_SR_FE |
                     USART_SR_ORE)) {
#ifdef STM32F1
    (void)usart_recv(UART_DEV);
#else
    usart_clear_flag(UART_DEV,
                     USART_ICR_NCF |
                     USART_ICR_FECF |
                     USART_ICR_ORECF);
#endif
    length = 0;
    goto resume;
  }
  
  length = sizeof(buffer) - dma_count_data(DMA_DEV, DMA_RX_CH);
  proto_process(buffer, &length);
  
 resume:
  rx_disable();
  
  if (length) {
    tx_enable(length);
  } else {
    rx_enable();
  }
}
