#ifndef __PROTO_H__
#define __PROTO_H__ "proto.h"

typedef struct {
  uint8_t id;
} proto_opt_t;

typedef struct {
  int16_t photo;
  int16_t therm;
  int16_t humid;
  int16_t tmcu;
  int16_t vref;
} proto_val_t;

typedef enum {
  proto_get_opt,
  proto_set_opt,
  proto_get_val,
} proto_op_t;

typedef enum {
  proto_success,
  proto_invalid,
} proto_st_t;

typedef struct {
  uint8_t id;
  uint8_t op;
  union {
    proto_opt_t opt;
  };
} proto_req_t;

typedef struct {
  uint8_t st;
  uint8_t op;
  union {
    proto_opt_t opt;
    proto_val_t val;
  };
} proto_res_t;

#define proto_length(t, f) ((size_t)(&((t*)0)->f)+sizeof(((t*)0)->f))

#ifdef PROTO_DEV

extern proto_opt_t *_param_start;
#define dev_opt (*_param_start)
//extern proto_opt_t dev_opt;
extern proto_val_t dev_val;

void proto_process(uint8_t *ptr, uint16_t *len);

#else /* PROTO_DEV */

#define proto_req_ptr(ptr) ((proto_req_t*)(ptr))
void proto_set_req(uint8_t *ptr, uint16_t *len);

#define proto_res_ptr(ptr) ((const proto_res_t*)(ptr))
uint8_t proto_get_res(const uint8_t *ptr, uint16_t *len);

#endif /* PROTO_DEV */

#endif /* __PROTO_H__ */
