libgroundsensor_proto.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libgroundsensor_proto
libgroundsensor_proto.INHERIT := firmware libopencm3 libsystem
libgroundsensor_proto.CDEFS := PROTO_CHECK $(if $(libgroundsensor_proto.device),PROTO_DEV,)
libgroundsensor_proto.CDIRS := $(libgroundsensor_proto.BASEPATH)include
libgroundsensor_proto.SRCS := $(addprefix $(libgroundsensor_proto.BASEPATH)src/, \
  proto.c)
