#include <libopencm3/stm32/flash.h>

#include <string.h>

#include "crc.h"
#include "proto.h"

static inline void
packet_wrap(uint8_t *ptr,
            uint16_t *len) {
  uint32_t sum;
  
  crc32_reset();

  *len += sizeof(sum);
  
  sum = crc32_update(ptr, *len);
  
  memcpy(&ptr[*len], &sum, sizeof(sum));
}

static inline uint8_t
packet_unwrap(const uint8_t *ptr,
              uint16_t *len) {
  if (*len < 4) {
    return 0;
  }

  uint32_t sum;

  memcpy(&sum, &ptr[*len], sizeof(sum));
  
  crc32_reset();
  
  *len -= sizeof(uint32_t);
  
  return sum == crc32_update(ptr, *len);
}

#ifdef PROTO_DEV

static inline void
proto_exchange(uint8_t *ptr, uint16_t *len) {
  const proto_req_t *req = (const proto_req_t*)ptr;
  proto_res_t *res = (proto_res_t*)ptr;
  
  if (req->id != 0 &&
      req->id != dev_opt.id) {
    *len = 0;
    return;
  }
  
  switch (req->op) {
  case proto_get_opt:
#ifdef PROTO_CHECK
    if (*len != proto_length(proto_req_t, op)) {
      *len = 0;
      break;
    }
#endif
    res->st = proto_success;
    res->opt = dev_opt;
    *len = proto_length(proto_res_t, opt);
    break;
  case proto_set_opt:
#ifdef PROTO_CHECK
    if (*len != proto_length(proto_req_t, opt)) {
      *len = 0;
      break;
    }
#endif
    flash_unlock();
    flash_erase_page((uint32_t)&dev_opt);
    { /* flashing options */
      const uint32_t *dst = (const uint32_t*)&dev_opt;
      const uint32_t *src = (const uint32_t*)&req->opt;
      const uint32_t *end = (const uint32_t*)&req->opt + sizeof(req->opt);
      for (; src < end; )
        flash_program_word((uint32_t)dst++, *src++);
    }
    flash_lock();
    res->st = memcmp(&dev_opt, &req->opt, sizeof(req->opt)) ? proto_invalid : proto_success;
    *len = proto_length(proto_res_t, st);
    break;
  case proto_get_val:
#ifdef PROTO_CHECK
    if (*len != proto_length(proto_req_t, op)) {
      *len = 0;
      break;
    }
#endif
    res->st = proto_success;
    res->val = dev_val;
    *len = proto_length(proto_res_t, val);
    break;
  }
}

void proto_process(uint8_t *ptr, uint16_t *len) {
  if (packet_unwrap(ptr, len)) {
    proto_exchange(ptr, len);
    if (*len) {
      packet_wrap(ptr, len);
    }
  } else {
    *len = 0;
  }
}

#else /* PROTO_DEV */

void proto_set_req(uint8_t *ptr, uint16_t *len) {
  proto_req_t *req = (proto_req_t*)ptr;

  switch (req->op) {
  case proto_get_opt:
    *len = proto_length(proto_req_t, op);
    break;
  case proto_set_opt:
    *len = proto_length(proto_req_t, opt);
    break;
  case proto_get_val:
    *len = proto_length(proto_req_t, op);
    break;
  }
  
  packet_wrap(ptr, len);
}

uint8_t proto_get_res(const uint8_t *ptr, uint16_t *len) {
  if (!packet_unwrap(ptr, len)) {
    return 0;
  }

#ifdef PROTO_CHECK
  const proto_res_t *res = (const proto_res_t*)ptr;
  
  switch (res->op) {
  case proto_get_opt:
    return *len == proto_length(proto_res_t, opt);
  case proto_set_opt:
    return *len == proto_length(proto_res_t, st);
  case proto_get_val:
    return *len == proto_length(proto_res_t, val);
  default:
    return 0;
  }
#else
  return 1;
#endif
}

#endif /* PROTO_DEV */
