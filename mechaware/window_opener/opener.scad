use <motor_28BYJ-48.scad>
use <square_profile.scad>
use <MCAD/nuts_and_bolts.scad>

module shaft(
    diameter = 8,
    length = 20,
){
    cylinder(r = diameter/2, h = length);
}

module frame(
    width=1100,
    height=250,
    tube=[20, 20],
){
    difference(){
        translate([-width/2, -tube[0], -height])
        cube([width, tube[0], height]);
        
        translate([-width/2+tube[1], -tube[0]-$fx, -height+tube[1]])
        cube([width-tube[1]*2, tube[0]+$fx*2, height-tube[1]*2]);
    }
}

$fn=32;
$fx=0.1;

a=$t*80;

mirror([0, 1, 0])
translate([0, -20, 0]){
    translate([1100/2-10, 20, 22])
    frame(1100, 250);

    rotate([-a, 0, 0]){
        translate([1100/2-10, 20, -3])
        frame(1050, 200);
    }
}

translate([0, 20, -0])
rotate([a, 0, 0])
translate([0, -20, 0])
/* leg */
translate([25, -20, -24])
difference(){
    translate([0, 0, -10])
    square_profile([20, 40, 80]);
    
    translate([0, -25, 24])
    rotate([9, 0, 0])
    cube([30, 26, 74], center = true);
    
    translate([0, 26, 60])
    rotate([9, 0, 0])
    cube([30, 20, 30], center = true);

    translate([-15, -5, 55])
    rotate([0, 90, 0])
    shaft(diameter = 9, length = 30);    
}

translate([0, 0, -100]){
    translate([-20, -28, 10])
    difference(){
        translate([20, 8, -15])
        square_profile([20, 40, 30]);
        
        for(m = [0, 1])
        mirror([0, 0, m])
        translate([20, -10, 15])
        rotate([-45, 0, 0])
        cube([30, 10, 30], center = true);
        
        rotate([0, 90, 0])
        shaft(diameter = 9, length = 40);
    }

    translate([0, -28, 10])
    rotate([8+6*sin($t*270-30), 0, 0]){
        translate([30, 8, -10])
        rotate([0, 0, 45])
        motor_28BYJ_48();
        
        difference(){
            translate([30, -12, 0])
            rotate([0, 90, 90])
            square_profile([20, 40, 37]);
    
            translate([30, 8, -10])
            rotate([0, 0, 45])
            motor_28BYJ_48(mount = 5);
    
            rotate([0, 90, 0])
            shaft(diameter = 9, length = 60);
    
            translate([24.5, 13.5, 5])
            rotate([0, 0, 0])
            shaft(diameter = 9, length = 10);
        }
    
        translate([24.5, 13.5, -7])
        rotate([0, 0, a*20]){
            translate([0, 0, 8])
            nutHole(size = 8);

            translate([0, 0, 1.5])
            rotate([0, 0, 90])
            nutHole(size = 8);

            translate([0, 0, 24])
            nutHole(size = 8);

            translate([0, 0, 17.5])
            rotate([0, 0, 90])
            nutHole(size = 8);

            translate([0, 0, 99])
            rotate([0, 0, 90])
            nutHorn(size = 8);
            
            shaft(diameter = 8, length = 140);
        }
        
        translate([-20, 0, 0])
        rotate([0, 90, 0])
        shaft(diameter = 8, length = 80);
    }
}
