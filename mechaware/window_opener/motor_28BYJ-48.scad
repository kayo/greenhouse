/* 28BYJ-48 Stepper Motor */

module motor_28BYJ_48(
    mount = 0,
    
    $fn = 32,
){
    if(mount){ /* render  mounting holes */
        /* motor shaft base ring */
		    translate([0, 8, -1])
        cylinder(r = 10/2, h = mount + 2);

        /* mounting lugs */
        for(m = [0, 1])
        mirror([m, 0, 0])
		    translate([-35/2, 0, -1])
        cylinder(r = 4.2/2, h = mount + 2);
    }else{ /* render motor model */
        /* motor body */
        translate([0, 0, -19])
        cylinder(r = 28/2, h = 19, $fn = $fn * 2);
        
		    /* motor shaft base ring */
		    translate([0, 8, 0])
        cylinder(r = 9/2, h = 1.5);
        
		    /* motor shaft */
		    translate([0, 8, 1.5])
		    intersection(){
		        cylinder(r = 5/2, h = 9);
		        cube([3, 6, 9], center = true);
		    }
        
        /* mounting lugs */
        for(m = [0, 1])
        mirror([m, 0, 0])
		    translate([-35/2, 0, -0.5])
		    difference(){
		        hull(){
			          cylinder(r = 7/2, h = 0.5);
			          translate([0, -7/2, 0])
                cube([7, 7, 0.5]);
		        }
		        translate([0, 0, -1])
            cylinder(r = 4.2/2, h = 2);
	      }
		    
		    /* wires housing */
		    translate([-14.6/2, -17, -17])
        cube([14.6, 6, 17]);
    }
}

motor_28BYJ_48();
motor_28BYJ_48(mount = 4);
