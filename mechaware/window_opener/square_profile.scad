/* Square Profile generator */

module square_profile(
    size = [20, 20, 50],
    wall = 1.6,
    bevel = 2,
    $fn = 64
){
    isize = [size[0]-wall*2, size[1]-wall*2, size[2]+wall*2];
    ibevel = max(0, bevel - wall);
    difference(){
        /* outer surface */
        translate([-size[0]/2+bevel, -size[1]/2+bevel, 0])
        minkowski(){
            cube([size[0]-bevel*2, size[1]-bevel*2, size[2]/2]);
            cylinder(r = bevel, h = size[2]/2, $fn = $fn/4);
        }
        /* inner surface */
        translate([-isize[0]/2+ibevel, -isize[1]/2+ibevel, -wall])
        minkowski(){
            cube([isize[0]-ibevel*2, isize[1]-ibevel*2, isize[2]/2]);
            cylinder(r = ibevel, h = isize[2]/2, $fn = $fn/8);
        }
    }
}


//cube([20, 20, 50], center = true);

translate([0, 0, -25])
square_profile([20, 40, 50]);
